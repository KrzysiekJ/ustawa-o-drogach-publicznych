======================================================
 Ustawa z dnia 21 marca 1985 r. o drogach publicznych
======================================================

Rozdział 1 — Przepisy ogólne
****************************

Art.1
-----

Drogą publiczną jest droga zaliczona na podstawie niniejszej ustawy do jednej z
kategorii dróg, z której może korzystać każdy, zgodnie z jej przeznaczeniem, z
ograniczeniami i wyjątkami określonymi w tej ustawie lub innych przepisach
szczególnych.

Art.2
-----

1. Drogi publiczne ze względu na funkcje w sieci drogowej dzielą się na
   następujące kategorie:

   1) drogi krajowe;
   2) drogi wojewódzkie;
   3) drogi powiatowe;
   4) drogi gminne.

2. Ulice leżące w ciągu dróg wymienionych w ust. 1 należą do tej samej
   kategorii co te drogi.
3. Drogi publiczne ze względów funkcjonalno-technicznych dzielą się na klasy
   określone w warunkach technicznych, o których mowa w art. 7 ust. 1 pkt 1
   ustawy z dnia 7 lipca 1994 r. - Prawo budowlane (Dz. U. z 2013 r. poz. 1409,
   z późn. zm.), jakim powinny odpowiadać drogi publiczne i ich usytuowanie.

Art.2a
------

1. Drogi krajowe stanowią własność Skarbu Państwa.
2. Drogi wojewódzkie, powiatowe i gminne stanowią własność właściwego samorządu
   województwa, powiatu lub gminy.

Art.3
-----

Drogi publiczne ze względu na ich dostępność dzielą się na:

1) drogi ogólnodostępne;
2) drogi o ograniczonej dostępności, w tym autostrady i drogi ekspresowe.

Art.4
-----

Użyte w ustawie określenia oznaczają:

1) pas drogowy - wydzielony liniami granicznymi grunt wraz z przestrzenią nad i
   pod jego powierzchnią, w którym są zlokalizowane droga oraz obiekty
   budowlane i urządzenia techniczne związane z prowadzeniem, zabezpieczeniem i
   obsługą ruchu, a także urządzenia związane z potrzebami zarządzania drogą;
2) droga - budowlę wraz z drogowymi obiektami inżynierskimi, urządzeniami oraz
   instalacjami, stanowiącą całość techniczno-użytkową, przeznaczoną do
   prowadzenia ruchu drogowego, zlokalizowaną w pasie drogowym;
3) ulica - drogę na terenie zabudowy lub przeznaczonym do zabudowy zgodnie z
   przepisami o planowaniu i zagospodarowaniu przestrzennym, w której ciągu
   może być zlokalizowane torowisko tramwajowe;
4) torowisko tramwajowe - część ulicy między skrajnymi szynami wraz z
   zewnętrznymi pasami bezpieczeństwa o szerokości 0,5 m każdy;
5) jezdnia - część drogi przeznaczoną do ruchu pojazdów;
6) chodnik - część drogi przeznaczoną do ruchu pieszych;
7) korona drogi - jezdnie z poboczami, pasami awaryjnego postoju lub pasami
   przeznaczonymi do ruchu pieszych, zatokami autobusowymi lub postojowymi, a
   przy drogach dwujezdniowych - również z pasem dzielącym jezdnie;
8) zjazd - połączenie drogi publicznej z nieruchomością położoną przy drodze,
   stanowiące bezpośrednie miejsce dostępu do drogi publicznej w rozumieniu
   przepisów o planowaniu i zagospodarowaniu przestrzennym;
9) skrzyżowanie dróg publicznych:

   a) jednopoziomowe - przecięcie się lub połączenie dróg publicznych na jednym
      poziomie,
   b) wielopoziomowe - krzyżowanie się lub połączenie dróg publicznych na
      różnych poziomach, zapewniające pełną lub częściową możliwość wyboru
      kierunku jazdy (węzeł drogowy) lub krzyżowanie się dróg na różnych
      poziomach, uniemożliwiające wybór kierunku jazdy (przejazd drogowy);
10) droga ekspresowa - drogę przeznaczoną wyłącznie do ruchu pojazdów
    samochodowych:

    a) wyposażoną w jedną lub dwie jezdnie,
    b) posiadającą wielopoziomowe skrzyżowania z przecinającymi ją innymi
       drogami transportu lądowego i wodnego, z dopuszczeniem wyjątkowo
       jednopoziomowych skrzyżowań z drogami publicznymi,
    c) wyposażoną w urządzenia obsługi podróżnych, pojazdów i przesyłek,
       przeznaczone wyłącznie dla użytkowników drogi;

11) autostrada - drogę przeznaczoną wyłącznie do ruchu pojazdów samochodowych:

    a) wyposażoną przynajmniej w dwie trwale rozdzielone jednokierunkowe
       jezdnie,
    b) posiadającą wielopoziomowe skrzyżowania ze wszystkimi przecinającymi ją
       drogami transportu lądowego i wodnego,
    c) wyposażoną w urządzenia obsługi podróżnych, pojazdów i przesyłek,
       przeznaczone wyłącznie dla użytkowników autostrady;

11) a)droga rowerowa - drogę przeznaczoną do ruchu rowerów albo rowerów i
    pieszych, z której może korzystać każdy, zgodnie z jej przeznaczeniem;
12) drogowy obiekt inżynierski - obiekt mostowy, tunel, przepust i konstrukcję
    oporową;
13) obiekt mostowy - budowlę przeznaczoną do przeprowadzenia drogi,
    samodzielnego ciągu pieszego lub pieszo-rowerowego, szlaku wędrówek
    zwierząt dziko żyjących lub innego rodzaju komunikacji nad przeszkodą
    terenową, w szczególności: most, wiadukt, estakadę, kładkę;
14) tunel - budowlę przeznaczoną do przeprowadzenia drogi, samodzielnego ciągu
    pieszego lub pieszo-rowerowego, szlaku wędrówek zwierząt dziko żyjących lub
    innego rodzaju komunikacji przez przeszkodę terenową lub pod nią, w tym
    przejście podziemne;
15) przepust - budowlę o przekroju poprzecznym zamkniętym, przeznaczoną do
    przeprowadzenia cieków, szlaków wędrówek zwierząt dziko żyjących lub
    urządzeń technicznych przez nasyp drogi;

15) a) kanał technologiczny - ciąg osłonowych elementów obudowy, studni
    kablowych oraz innych obiektów lub urządzeń służących umieszczeniu lub
    eksploatacji:

    a) urządzeń infrastruktury technicznej związanych z potrzebami zarządzania
       drogami lub potrzebami ruchu drogowego,
    b) linii telekomunikacyjnych wraz z zasilaniem oraz linii
       elektroenergetycznych, niezwiązanych z potrzebami zarządzania drogami
       lub potrzebami ruchu drogowego;

16) konstrukcja oporowa - budowlę przeznaczoną do utrzymywania w stanie
    stateczności nasypu lub wykopu;
17) budowa drogi - wykonywanie połączenia drogowego między określonymi
    miejscami lub miejscowościami, a także jego odbudowę i rozbudowę;
18) przebudowa drogi - wykonywanie robót, w których wyniku następuje
    podwyższenie parametrów technicznych i eksploatacyjnych istniejącej drogi,
    niewymagających zmiany granic pasa drogowego;
19) remont drogi - wykonywanie robót przywracających pierwotny stan drogi,
    także przy użyciu wyrobów budowlanych innych niż użyte w stanie pierwotnym;
20) utrzymanie drogi - wykonywanie robót konserwacyjnych, porządkowych i innych
    zmierzających do zwiększenia bezpieczeństwa i wygody ruchu, w tym także
    odśnieżanie i zwalczanie śliskości zimowej;
21) ochrona drogi - działania mające na celu niedopuszczenie do przedwczesnego
    zniszczenia drogi, obniżenia klasy drogi, ograniczenia jej funkcji,
    niewłaściwego jej użytkowania oraz pogorszenia warunków bezpieczeństwa
    ruchu;
22) zieleń przydrożna - roślinność umieszczoną w pasie drogowym, mającą na celu
    w szczególności ochronę użytkowników drogi przed oślepianiem przez pojazdy
    nadjeżdżające z kierunku przeciwnego, ochronę drogi przed zawiewaniem i
    zaśnieżaniem, ochronę przyległego terenu przed nadmiernym hałasem,
    zanieczyszczeniem powietrza, wody i gleby;
23) reklama - nośnik informacji wizualnej w jakiejkolwiek materialnej formie
    wraz z elementami konstrukcyjnymi i zamocowaniami, umieszczony w polu
    widzenia użytkowników drogi, niebędący znakiem w rozumieniu przepisów o
    znakach i sygnałach lub znakiem informującym o obiektach użyteczności
    publicznej ustawionym przez gminę;
24) dostępność drogi - cechę charakteryzującą gęstość połączeń danej drogi z
    innymi drogami przez skrzyżowania dróg oraz zakres dostępu do drogi przez
    zjazdy;
25) pojazd nienormatywny - pojazd lub zespół pojazdów w rozumieniu art. 2 pkt
    35a ustawy z dnia 20 czerwca 1997 r. - Prawo o ruchu drogowym (Dz. U. z
    2012 r. poz. 1137, z późn. zm.);
26) transeuropejska sieć drogowa - sieć drogową określoną w decyzji Parlamentu
    Europejskiego i Rady nr 661/2010/UE z dnia 7 lipca 2010 r. w sprawie
    unijnych wytycznych dotyczących rozwoju transeuropejskiej sieci
    transportowej (wersja przekształcona) (Dz. Urz. UE L 204 z 05.08.2010,
    str. 1);
27) służby ratownicze - jednostki ochrony przeciwpożarowej w rozumieniu ustawy
    z dnia 24 sierpnia 1991 r. o ochronie przeciwpożarowej (Dz. U. z 2009 r. Nr
    178, poz. 1380, z późn. zm.) oraz zespoły ratownictwa medycznego w
    rozumieniu ustawy z dnia 8 września 2006 r. o Państwowym Ratownictwie
    Medycznym (Dz. U. z 2013 r. poz. 757, z późn. zm.);
28) ocena wpływu planowanej drogi na bezpieczeństwo ruchu drogowego -
    strategiczną analizę wpływu wariantów planowanej drogi na poziom
    bezpieczeństwa ruchu drogowego w sieci dróg publicznych znajdujących się w
    obszarze oddziaływania planowanej drogi;
29) audyt bezpieczeństwa ruchu drogowego - niezależną, szczegółową, techniczną
    ocenę cech projektowanej, budowanej, przebudowywanej lub użytkowanej drogi
    publicznej pod względem bezpieczeństwa uczestników ruchu drogowego;
30) klasyfikacja odcinków dróg ze względu na koncentrację wypadków
    śmiertelnych - analizę istniejącej sieci drogowej pod względem liczby
    wypadków śmiertelnych, w wyniku której wytypowane zostają najbardziej
    niebezpieczne odcinki dróg o dużej liczbie wypadków śmiertelnych;
31) klasyfikacja odcinków dróg ze względu na bezpieczeństwo sieci drogowej -
    analizę istniejącej sieci drogowej, w wyniku której wytypowane zostają
    odcinki dróg o dużej możliwości poprawy bezpieczeństwa oraz zmniejszenia
    kosztów wypadków drogowych;
32) uczestnik ruchu drogowego - uczestnika ruchu drogowego w rozumieniu ustawy
    z dnia 20 czerwca 1997 r. - Prawo o ruchu drogowym;
33) inteligentne systemy transportowe (ITS) - systemy wykorzystujące
    technologie informacyjne i komunikacyjne w obszarze transportu drogowego,
    obejmującym infrastrukturę, pojazdy i jego użytkowników, a także w
    obszarach zarządzania ruchem i zarządzania mobilnością, oraz do interfejsów
    z innymi rodzajami transportu;
34) interoperacyjność - zdolność systemów oraz będących ich podstawą procesów
    gospodarczych do wymiany danych, informacji i wiedzy;
35) aplikacja ITS - operacyjne narzędzie zastosowania ITS;
36) usługa ITS - dostarczanie aplikacji ITS w określonych ramach
    organizacyjnych i operacyjnych, w celu zwiększenia bezpieczeństwa
    użytkowników w obszarze transportu drogowego, efektywności i wygody ich
    przemieszczania się, a także ułatwiania lub wspierania operacji
    transportowych i przewozowych;
37) użytkownik ITS - każdego użytkownika aplikacji lub usług ITS, w tym
    podróżnych, szczególnie zagrożonych uczestników ruchu drogowego,
    użytkowników i zarządców dróg, podmioty zarządzające pojazdami w
    transporcie osób lub rzeczy oraz służby ustawowo powołane do niesienia
    pomocy;
38) szczególnie zagrożeni uczestnicy ruchu drogowego - niezmotoryzowanych
    uczestników ruchu drogowego, w szczególności pieszych i rowerzystów, a
    także motocyklistów oraz osoby niepełnosprawne lub osoby o widocznej
    ograniczonej sprawności ruchowej;
39) interfejs - połączenie między systemami, które zapewnia ich łączenie i
    współpracę;
40) ciągłość usług - zdolność do zapewnienia nieprzerwanych usług w ramach
    sieci transportowych na obszarze Unii Europejskiej.

Art.4a
------

1. Rada Ministrów określi, w drodze rozporządzenia, sieć autostrad i dróg
   ekspresowych, mając na uwadze potrzeby społeczne i gospodarcze kraju w
   zakresie rozwoju infrastruktury.
2. Minister właściwy do spraw transportu w porozumieniu z Ministrem Obrony
   Narodowej określi, w drodze zarządzenia, wykaz dróg o znaczeniu
   obronnym. Zarządzenie to nie podlega ogłoszeniu.

Art.5
-----

1. Do dróg krajowych zalicza się:

   1) autostrady i drogi ekspresowe oraz drogi leżące w ich ciągach do czasu
      wybudowania autostrad i dróg ekspresowych;
   2) drogi międzynarodowe;
   3) drogi stanowiące inne połączenia zapewniające spójność sieci dróg
      krajowych;
   4) drogi dojazdowe do ogólnodostępnych przejść granicznych obsługujących
      ruch osobowy i towarowy bez ograniczeń ciężaru całkowitego pojazdów
      (zespołu pojazdów) lub wyłącznie ruch towarowy bez ograniczeń ciężaru
      całkowitego pojazdów (zespołu pojazdów);
   5) drogi alternatywne dla autostrad płatnych;
   6) drogi stanowiące ciągi obwodnicowe dużych aglomeracji miejskich;
   7) drogi o znaczeniu obronnym.

2. Minister właściwy do spraw transportu w porozumieniu z ministrami właściwymi
   do spraw administracji publicznej, spraw wewnętrznych oraz Ministrem Obrony
   Narodowej, po zasięgnięciu opinii właściwych sejmików województw, a w
   miastach na prawach powiatu - opinii rad miast, w drodze rozporządzenia,
   zalicza drogi do kategorii dróg krajowych, mając na uwadze kryteria
   zaliczenia określone w ust. 1.
3. Minister właściwy do spraw transportu po zasięgnięciu opinii właściwych
   zarządów województw, a w miastach na prawach powiatu - właściwych
   prezydentów miast, w drodze rozporządzenia, ustala przebieg istniejących
   dróg krajowych, w celu zapewnienia ciągłości dróg krajowych.

Art.6
-----

1. Do dróg wojewódzkich zalicza się drogi inne niż określone w art. 5 ust. 1,
   stanowiące połączenia między miastami, mające znaczenie dla województwa, i
   drogi o znaczeniu obronnym niezaliczone do dróg krajowych.
2. Zaliczenie do kategorii dróg wojewódzkich następuje w drodze uchwały sejmiku
   województwa w porozumieniu z ministrami właściwymi do spraw transportu oraz
   obrony narodowej.
3. Ustalenie przebiegu istniejących dróg wojewódzkich następuje w drodze
   uchwały sejmiku województwa, po zasięgnięciu opinii zarządów powiatów, na
   obszarze których przebiega droga, a w miastach na prawach powiatu - opinii
   prezydentów miast.

Art.6a
------

1. Do dróg powiatowych zalicza się drogi inne niż określone w art. 5 ust. 1 i
   art. 6 ust. 1, stanowiące połączenia miast będących siedzibami powiatów z
   siedzibami gmin i siedzib gmin między sobą.
2. Zaliczenie drogi do kategorii dróg powiatowych następuje w drodze uchwały
   rady powiatu w porozumieniu z zarządem województwa, po zasięgnięciu opinii
   wójtów (burmistrzów, prezydentów miast) gmin, na obszarze których przebiega
   droga, oraz zarządów sąsiednich powiatów, a w miastach na prawach powiatu -
   opinii prezydentów miast.
3. Ustalenie przebiegu istniejących dróg powiatowych następuje w drodze uchwały
   rady powiatu, po zasięgnięciu opinii wójtów (burmistrzów, prezydentów miast)
   gmin, na obszarze których przebiega droga.

Art.7
-----

1. Do dróg gminnych zalicza się drogi o znaczeniu lokalnym niezaliczone do
   innych kategorii, stanowiące uzupełniającą sieć dróg służących miejscowym
   potrzebom, z wyłączeniem dróg wewnętrznych.
2. Zaliczenie do kategorii dróg gminnych następuje w drodze uchwały rady gminy
   po zasięgnięciu opinii właściwego zarządu powiatu.
3. Ustalenie przebiegu istniejących dróg gminnych następuje w drodze uchwały
   rady gminy.

Art.7a
------

1. Organy właściwe w sprawach zaliczenia do kategorii i ustalenia przebiegu
   dróg, przekazując propozycje zaliczenia do kategorii lub propozycje
   ustalenia przebiegu dróg, wyznaczają termin do przedstawienia opinii, o
   których mowa w art. 5-7. Wyznaczony termin zgłaszania opinii nie może być
   krótszy niż 21 dni od dnia doręczenia propozycji do zaopiniowania.
2. Niezłożenie opinii w przewidzianym terminie uznaje się za akceptację
   propozycji.

Art.8
-----

1. Drogi, drogi rowerowe, parkingi oraz place przeznaczone do ruchu pojazdów,
   niezaliczone do żadnej z kategorii dróg publicznych i niezlokalizowane w
   pasie drogowym tych dróg są drogami wewnętrznymi.  1a.Podjęcie przez radę
   gminy uchwały w sprawie nadania nazwy drodze wewnętrznej wymaga uzyskania
   pisemnej zgody właścicieli terenów, na których jest ona zlokalizowana.
2. Budowa, przebudowa, remont, utrzymanie, ochrona i oznakowanie dróg
   wewnętrznych oraz zarządzanie nimi należy do zarządcy terenu, na którym jest
   zlokalizowana droga, a w przypadku jego braku - do właściciela tego terenu.
3. Finansowanie zadań, o których mowa w ust. 2, należy do zarządcy terenu, na
   którym jest zlokalizowana droga, a w przypadku jego braku - do właściciela
   tego terenu.
4. Oznakowanie połączeń dróg wewnętrznych z drogami publicznymi oraz utrzymanie
   urządzeń bezpieczeństwa i organizacji ruchu, związanych z funkcjonowaniem
   tych połączeń, należy do zarządcy drogi publicznej.

Art.9
-----

(uchylony)

Art.10
------

1. Organem właściwym do pozbawienia drogi dotychczasowej kategorii jest organ
   właściwy do zaliczenia jej do odpowiedniej kategorii.
2. Pozbawienia drogi jej kategorii dokonuje się w trybie właściwym do
   zaliczenia drogi do odpowiedniej kategorii.
3. Pozbawienie drogi dotychczasowej kategorii, z wyjątkiem przypadku wyłączenia
   drogi z użytkowania, jest możliwe jedynie w sytuacji jednoczesnego
   zaliczenia tej drogi do nowej kategorii. Pozbawienie i zaliczenie nie może
   być dokonane później niż do końca trzeciego kwartału danego roku, z mocą od
   dnia 1 stycznia roku następnego.
4. Nowo wybudowany odcinek drogi zostaje zaliczony do kategorii drogi, w której
   ciągu leży.
5. Odcinek drogi zastąpiony nowo wybudowanym odcinkiem drogi z chwilą oddania
   go do użytkowania zostaje pozbawiony dotychczasowej kategorii i zaliczony do
   kategorii drogi gminnej.
6. Drogom publicznym, obiektom mostowym i tunelom nadaje się numerację.
7. Numery drogom, po zaliczeniu ich do kategorii dróg publicznych, nadają
   odpowiednio:

   1) drogom krajowym i wojewódzkim - Generalny Dyrektor Dróg Krajowych i
      Autostrad;
   2) drogom powiatowym i gminnym - zarządy województw.

8. Generalny Dyrektor Dróg Krajowych i Autostrad oraz zarządy województw
   prowadzą rejestry numerów nadanych drogom.
9. Jednolite numery inwentarzowe, zwane dalej "JNI", obiektom mostowym i
   tunelom nadaje, na podstawie zgłoszeń zarządców dróg, Generalny Dyrektor
   Dróg Krajowych i Autostrad.
10. Generalny Dyrektor Dróg Krajowych i Autostrad prowadzi rejestr JNI nadanych
    obiektom mostowym i tunelom.
11. Dla poszczególnych kategorii dróg właściwy zarządca drogi prowadzi
    ewidencję dróg, obiektów mostowych, tuneli, przepustów i promów.
12. Minister właściwy do spraw transportu określi, w drodze rozporządzenia,
    sposób numeracji oraz zakres, treść i sposób prowadzenia ewidencji dróg
    publicznych, obiektów mostowych, tuneli, przepustów i promów, a także treść
    i sposób prowadzenia rejestrów numerów nadanych drogom oraz rejestru JNI
    nadanych obiektom mostowym i tunelom, mając na względzie potrzeby
    zarządzania drogami publicznymi oraz gromadzenia danych o sieci tych dróg,
    w ramach jednolitej metodyki systemu referencyjnego.

Art.11
------

(uchylony)

Art.12
------

(uchylony)

Art.12a
-------

1. Organ właściwy do zarządzania ruchem na drogach wyznaczając miejsca
   przeznaczone na postój pojazdów wyznacza stanowiska postojowe dla pojazdów
   zaopatrzonych w kartę parkingową, o której mowa w art. 8 ustawy z dnia 20
   czerwca 1997 r. - Prawo o ruchu drogowym, zwaną dalej "kartą parkingową":

   1) na drogach publicznych;
   2) w strefach zamieszkania, o których mowa w art. 2 pkt 16 ustawy z dnia 20
      czerwca 1997 r. - Prawo o ruchu drogowym;
   3) w strefach ruchu, o których mowa w art. 2 pkt 16a ustawy z dnia 20
      czerwca 1997 r. - Prawo o ruchu drogowym.
2. Stanowiska postojowe, o których mowa w ust. 1, w miejscu przeznaczonym na
   postój pojazdów wyznacza się w liczbie nie mniejszej niż:

   1) 1 stanowisko - jeżeli liczba stanowisk wynosi 6-15;
   2) 2 stanowiska - jeżeli liczba stanowisk wynosi 16-40;
   3) 3 stanowiska - jeżeli liczba stanowisk wynosi 41-100;
   4) 4% ogólnej liczby stanowisk jeżeli ogólna liczba stanowisk wynosi więcej
      niż 100.

Art.13
------

1. Korzystający z dróg publicznych są obowiązani do ponoszenia opłat za:

   1) postój pojazdów samochodowych na drogach publicznych w strefie płatnego
      parkowania;
   2) (uchylony)
   3) przejazdy po drogach krajowych pojazdów samochodowych, w rozumieniu
      art. 2 pkt 33 ustawy z dnia 20 czerwca 1997 r. - Prawo o ruchu drogowym,
      za które uważa się także zespół pojazdów składający się z pojazdu
      samochodowego oraz przyczepy lub naczepy o dopuszczalnej masie całkowitej
      powyżej 3,5 tony, w tym autobusów niezależnie od ich dopuszczalnej masy
      całkowitej.

2. Korzystający z dróg publicznych mogą być obowiązani do ponoszenia opłat za:

   1) przejazdy przez obiekty mostowe i tunele zlokalizowane w ciągach dróg
      publicznych;
   2) przeprawy promowe na drogach publicznych.

3. Od opłat, o których mowa w ust. 1 pkt 1 oraz ust. 2, są zwolnione:

   1) pojazdy:

      a) Policji, Inspekcji Transportu Drogowego, Agencji Bezpieczeństwa
         Wewnętrznego, Agencji Wywiadu, Służby Kontrwywiadu Wojskowego, Służby
         Wywiadu Wojskowego, Centralnego Biura Antykorupcyjnego, Straży
         Granicznej, Biura Ochrony Rządu, Służby Więziennej, Służby Celnej,
         służb ratowniczych,
      b) zarządów dróg,
      c) Sił Zbrojnych Rzeczypospolitej Polskiej, a także sił zbrojnych państw
         obcych, jeżeli umowa międzynarodowa, której Rzeczpospolita Polska jest
         stroną, tak stanowi,
      d) wykorzystywane w ratownictwie lub w przypadku klęski żywiołowej;

   2) autobusy szkolne przewożące dzieci do szkoły.

3. a) Od opłat, o których mowa w ust. 1 pkt 3, są zwolnione pojazdy:

   1) Sił Zbrojnych Rzeczypospolitej Polskiej, a także sił zbrojnych państw
      obcych, jeżeli umowa międzynarodowa, której Rzeczpospolita Polska jest
      stroną, tak stanowi;
   2) służb ratowniczych, służb ratownictwa górniczego, Morskiej Służby
      Poszukiwania i Ratownictwa, Straży Granicznej, Biura Ochrony Rządu,
      Służby Więziennej, Inspekcji Transportu Drogowego, Służby Celnej,
      Policji, Agencji Bezpieczeństwa Wewnętrznego, Agencji Wywiadu, Służby
      Kontrwywiadu Wojskowego, Służby Wywiadu Wojskowego oraz Centralnego Biura
      Antykorupcyjnego;
   3) zarządcy dróg krajowych wykorzystywane do utrzymania tych dróg.

4. Od opłat, o których mowa w ust. 2 pkt 2, zwolnione są:

   1) ciągniki rolnicze i inne maszyny rolnicze;
   2) pojazdy zaopatrzone w kartę parkingową.

5. Korzystający z drogi publicznej jest zwolniony z opłat, o których mowa w
   ust. 1 pkt 1 i ust. 2, w przypadku wykonywania przez niego przejazdu w
   ramach pomocy humanitarnej lub medycznej.
6. Zwolnienie, o którym mowa w ust. 5, następuje w drodze decyzji
   administracyjnej, wydanej przez ministra właściwego do spraw transportu, na
   wniosek podmiotu występującego o takie zwolnienie.

Art.13a
-------

Zadania w zakresie budowy i eksploatacji autostrad i dróg ekspresowych mogą być
realizowane:

1) na zasadach ogólnych określonych w ustawie;
2) na zasadach określonych w przepisach o autostradach płatnych oraz o Krajowym
   Funduszu Drogowym;
3) na zasadach określonych w ustawie z dnia 19 grudnia 2008 r. o partnerstwie
   publiczno-prywatnym (Dz. U. z 2009 r. Nr 19, poz. 100, z późn. zm.);
4) na zasadach określonych w ustawie z dnia 12 stycznia 2007 r. o drogowych
   spółkach specjalnego przeznaczenia (Dz. U. Nr 23, poz. 136, z późn. zm.);
5) na zasadach określonych w ustawie z dnia 9 stycznia 2009 r. o koncesji na
   roboty budowlane lub usługi (Dz. U. z 2015 r. poz. 113).

Art.13b
-------

1. Opłatę, o której mowa w art. 13 ust. 1 pkt 1, pobiera się za postój pojazdów
   samochodowych w strefie płatnego parkowania, w wyznaczonym miejscu, w
   określone dni robocze, w określonych godzinach lub całodobowo.
2. Strefę płatnego parkowania ustala się na obszarach charakteryzujących się
   znacznym deficytem miejsc postojowych, jeżeli uzasadniają to potrzeby
   organizacji ruchu, w celu zwiększenia rotacji parkujących pojazdów
   samochodowych lub realizacji lokalnej polityki transportowej, w
   szczególności w celu ograniczenia dostępności tego obszaru dla pojazdów
   samochodowych lub wprowadzenia preferencji dla komunikacji zbiorowej.
3. Rada gminy (rada miasta) na wniosek wójta (burmistrza, prezydenta miasta),
   zaopiniowany przez organy zarządzające drogami i ruchem na drogach, może
   ustalić strefę płatnego parkowania.
4. Rada gminy (rada miasta), ustalając strefę płatnego parkowania:

   1) ustala wysokość opłaty, o której mowa w art. 13 ust. 1 pkt 1, z tym że
      opłata za pierwszą godzinę postoju pojazdu samochodowego nie może
      przekraczać 3 zł;
   2) może wprowadzić opłaty abonamentowe lub zryczałtowane oraz zerową stawkę
      opłaty dla niektórych użytkowników drogi;
   3) określa sposób pobierania opłaty, o której mowa w art. 13 ust. 1 pkt 1.

5. Stawki opłat, o których mowa w art. 13 ust. 1 pkt 1, mogą być zróżnicowane w
   zależności od miejsca postoju. Przy ustalaniu stawek uwzględnia się
   progresywne narastanie opłaty przez pierwsze trzy godziny postoju, przy czym
   progresja nie może przekraczać powiększenia stawki opłaty o 20% za kolejne
   godziny w stosunku do stawki za poprzednią godzinę postoju. Stawka opłaty za
   czwartą godzinę i za kolejne godziny postoju nie może przekraczać stawki
   opłaty za pierwszą godzinę postoju.
6. Organ właściwy do zarządzania ruchem na drogach w uzgodnieniu z zarządcą
   drogi:

   1) wyznacza w strefie płatnego parkowania miejsca przeznaczone na postój
      pojazdów, w tym stanowiska przeznaczone na postój pojazdów zaopatrzonych
      w kartę parkingową;
   2) może wyznaczać w strefie płatnego parkowania zastrzeżone stanowiska
      postojowe (koperty) w celu korzystania z nich na prawach wyłączności w
      określonych godzinach lub całodobowo.

7. Opłatę, o której mowa w art. 13 ust. 1 pkt 1, pobiera zarząd drogi, a w
   przypadku jego braku - zarządca drogi.

Art.13c
-------

(uchylony)

Art.13d
-------

1. Opłata, o której mowa w art. 13 ust. 2 pkt 1, może być pobierana za przejazd
   pojazdów przez obiekt mostowy lub tunel, którego długość jest większa niż
   400 m.
2. Przez długość obiektu mostowego lub tunelu, o której mowa w ust. 1, należy
   rozumieć długość całkowitą obiektu podaną w dokumentach ewidencyjnych
   obiektu mostowego lub tunelu, prowadzonych zgodnie z przepisami w sprawie
   numeracji i ewidencji dróg, obiektów mostowych, tuneli i promów.
3. Ustala się następujące kategorie pojazdów w celu określania wysokości
   opłaty, o której mowa w art. 13 ust. 2 pkt 1:

   1) kategoria 1 - trójkołowe lub czterokołowe pojazdy samochodowe o masie
      własnej nieprzekraczającej 550 kg;
   2) kategoria 2 - pojazdy samochodowe o dopuszczalnej masie całkowitej
      nieprzekraczającej 3,5 t, z wyjątkiem autobusów, oraz zespoły tych
      pojazdów;
   3) kategoria 3 - autobusy o dwóch osiach, ciągniki samochodowe bez naczep;
   4) kategoria 4 - samochody ciężarowe o dopuszczalnej masie całkowitej
      przekraczającej 3,5 t, autobusy o trzech i więcej osiach, pojazdy
      członowe o całkowitej liczbie osi do pięciu;
   5) kategoria 5 - zespoły pojazdów składające się z samochodu ciężarowego o
      dopuszczalnej masie całkowitej przekraczającej 3,5 t i przyczepy albo z
      autobusu i przyczepy oraz pojazdy członowe o całkowitej liczbie osi
      powyżej pięciu, a także pojazdy specjalne.

4. Minister właściwy do spraw transportu, w drodze rozporządzenia, może
   wprowadzić dla obiektów mostowych zlokalizowanych w ciągach dróg, których
   zarządcą jest Generalny Dyrektor Dróg Krajowych i Autostrad albo dla których
   funkcję zarządcy drogi pełni drogowa spółka specjalnego przeznaczenia,
   opłatę, o której mowa w art. 13 ust. 2 pkt 1, i ustalić wysokość opłaty, z
   tym że jednorazowa opłata za przejazd przez obiekt mostowy nie może
   przekroczyć dla:

   1) pojazdu kategorii 1 - 4 zł;
   2) pojazdu kategorii 2 - 5 zł;
   3) pojazdu kategorii 3 - 6 zł;
   4) pojazdu kategorii 4 - 8 zł;
   5) pojazdu kategorii 5 - 9 zł.

5. Organ stanowiący jednostki samorządu terytorialnego, w drodze uchwały, może
   wprowadzić dla obiektów mostowych zlokalizowanych w ciągach dróg, których
   zarządcą jest jednostka samorządu terytorialnego, opłatę, o której mowa w
   art. 13 ust. 2 pkt 1, i ustalić wysokość opłaty, z tym że jednorazowa opłata
   za przejazd przez obiekt mostowy nie może przekroczyć stawek określonych w
   ust. 4 pkt 1-5.
6. Minister właściwy do spraw transportu, w drodze rozporządzenia, może
   wprowadzić dla tuneli zlokalizowanych w ciągach dróg, których zarządcą jest
   Generalny Dyrektor Dróg Krajowych i Autostrad albo dla których funkcję
   zarządcy drogi pełni drogowa spółka specjalnego przeznaczenia, opłatę, o
   której mowa w art. 13 ust. 2 pkt 1, i ustalić wysokość opłaty, z tym że
   jednorazowa opłata za przejazd przez tunel nie może przekroczyć dla:

   1) pojazdu kategorii 1 - 8 zł;
   2) pojazdu kategorii 2 - 10 zł;
   3) pojazdu kategorii 3 - 12 zł;
   4) pojazdu kategorii 4 - 15 zł;
   5) pojazdu kategorii 5 - 18 zł.

7. Organ stanowiący jednostki samorządu terytorialnego, w drodze uchwały, może
   wprowadzić dla tuneli zlokalizowanych w ciągach dróg, których zarządcą jest
   jednostka samorządu terytorialnego, opłatę, o której mowa w art. 13 ust. 2
   pkt 1, i ustalić wysokość opłaty, z tym że jednorazowa opłata za przejazd
   przez tunel nie może przekroczyć stawek, o których mowa w ust. 6 pkt 1-5.
8. Minister właściwy do spraw transportu wprowadzając opłatę, o której mowa w
   art. 13 ust. 2 pkt 1, w drodze rozporządzenia:

   1) może wprowadzić opłaty abonamentowe lub zryczałtowaną oraz zerową stawkę
      opłaty dla niektórych użytkowników drogi,
   2) określa sposób ogłaszania wysokości tej opłaty,
   3) określa sposób pobierania tej opłaty

   – uwzględniając w szczególności potrzeby użytkowników w zakresie
   częstotliwości korzystania z obiektów mostowych i tuneli, zmniejszenie
   skutków finansowych wprowadzenia opłaty dla mieszkańców, dostępność
   informacji o wysokości opłat oraz koszt poboru tej opłaty.

9. Organ stanowiący jednostki samorządu terytorialnego wprowadzając opłatę, o
   której mowa w art. 13 ust. 2 pkt 1, w drodze uchwały:

   1) może wprowadzić opłaty abonamentowe lub zryczałtowaną oraz zerową stawkę
      opłaty dla niektórych użytkowników drogi;
   2) określa sposób ogłaszania wysokości tej opłaty;
   3) określa sposób pobierania tej opłaty.

10. Opłatę, o której mowa w art. 13 ust. 2 pkt 1, pobiera zarząd drogi, a w
    przypadku jego braku zarządca drogi, z zastrzeżeniem ust. 10a.

10. a) Opłatę, o której mowa w art. 13 ust. 2 pkt 1, może pobierać na warunkach
    określonych w umowie, o której mowa w art. 6 ust. 1 ustawy z dnia 12
    stycznia 2007 r. o drogowych spółkach specjalnego przeznaczenia, drogowa
    spółka specjalnego przeznaczenia, która pełni funkcję zarządcy drogi
    krajowej.

Art.13e
-------

1. Opłata, o której mowa w art. 13 ust. 2 pkt 2, może być pobierana za przewóz
   promem osób, zwierząt, bagażu lub pojazdów.
2. Nie pobiera się opłaty za przewóz osób od kierowcy przewożonego pojazdu.
3. Nie pobiera się opłaty za przewóz zwierzęcia przewożonego jako bagaż ręczny.
4. Nie pobiera się opłaty za przewóz bagażu od osoby przewożącej bagaż ręczny,
   którego masa nie przekracza 20 kg na osobę.
5. Ustala się następujące kategorie pojazdów w celu określania wysokości
   opłaty, o której mowa w art. 13 ust. 2 pkt 2:

   1) kategoria 1 - trójkołowe lub czterokołowe pojazdy samochodowe o masie
      własnej nieprzekraczającej 550 kg, motorowery i motocykle;
   2) kategoria 2 - pojazdy samochodowe o dopuszczalnej masie całkowitej
      nieprzekraczającej 3,5 t, z wyjątkiem autobusów, oraz zespoły tych
      pojazdów, a także pojazdy zaprzęgowe;
   3) kategoria 3 - autobusy o dwóch osiach, ciągniki samochodowe bez naczep;
   4) kategoria 4 - samochody ciężarowe o dopuszczalnej masie całkowitej
      przekraczającej 3,5 t, autobusy o trzech i więcej osiach, pojazdy
      członowe o całkowitej liczbie osi do pięciu;
   5) kategoria 5 - zespoły pojazdów składające się z samochodu ciężarowego o
      dopuszczalnej masie całkowitej przekraczającej 3,5 t i przyczepy albo z
      autobusu i przyczepy oraz pojazdy członowe o całkowitej liczbie osi
      powyżej pięciu, a także pojazdy specjalne.
6. Minister właściwy do spraw transportu, w drodze rozporządzenia, może
   wprowadzić dla przepraw promowych zlokalizowanych w ciągach dróg, których
   zarządcą jest Generalny Dyrektor Dróg Krajowych i Autostrad, opłatę, o
   której mowa w art. 13 ust. 2 pkt 2, i ustalić wysokość opłaty, z tym że
   jednorazowa opłata za przewóz promem nie może przekroczyć dla:

   1) jednej osoby - 3 zł;
   2) jednego zwierzęcia - 3 zł;
   3) jednej sztuki bagażu - 5 zł;
   4) pojazdu:

      a) kategorii 1 - 8 zł,
      b) kategorii 2 - 10 zł,
      c) kategorii 3 - 12 zł,
      d) kategorii 4 - 15 zł,
      e) kategorii 5 - 18 zł.

7. Organ stanowiący jednostki samorządu terytorialnego, w drodze uchwały, może
   wprowadzić dla przepraw promowych zlokalizowanych w ciągach dróg, których
   zarządcą jest jednostka samorządu terytorialnego, opłatę, o której mowa w
   art. 13 ust. 2 pkt 2, i ustalić wysokość opłaty, z tym że jednorazowa opłata
   za przewóz promem nie może przekroczyć stawek, o których mowa w ust. 6 pkt
   1-4.
8. Minister właściwy do spraw transportu wprowadzając opłatę, o której mowa w
   art. 13 ust. 2 pkt 2, w drodze rozporządzenia:

   1) może wprowadzić opłaty abonamentowe lub zryczałtowaną oraz zerową stawkę
      opłaty dla niektórych użytkowników drogi,
   2) określa sposób ogłaszania wysokości tej opłaty,
   3) określa sposób pobierania tej opłaty

   – uwzględniając w szczególności potrzeby użytkowników w zakresie
   częstotliwości korzystania z przepraw promowych, zmniejszenie skutków
   finansowych wprowadzenia opłaty dla mieszkańców, dostępność informacji o
   wysokości opłat oraz koszt poboru tej opłaty.

9. Organ stanowiący jednostki samorządu terytorialnego wprowadzając opłatę, o
   której mowa w art. 13 ust. 2 pkt 2, w drodze uchwały:

   1) może wprowadzić opłaty abonamentowe lub zryczałtowaną oraz zerową stawkę
      opłaty dla niektórych użytkowników drogi;
   2) określa sposób ogłaszania wysokości tej opłaty;
   3) określa sposób pobierania tej opłaty.

10. Opłatę, o której mowa w art. 13 ust. 2 pkt 2, pobiera zarząd drogi, w
    której ciągu jest zlokalizowana przeprawa promowa, a w przypadku jego braku
    zarządca drogi.

Art.13f
-------

1. Za nieuiszczenie opłat, o których mowa w art. 13 ust. 1 pkt 1, pobiera się
   opłatę dodatkową.
2. Rada gminy (rada miasta) określa wysokość opłaty dodatkowej, o której mowa w
   ust. 1, oraz sposób jej pobierania. Wysokość opłaty dodatkowej nie może
   przekroczyć 50 zł.
3. Opłatę dodatkową, o której mowa w ust. 1, pobiera zarząd drogi, a w
   przypadku jego braku zarządca drogi.

Art.13g
-------

(uchylony)

Art.13h
-------

W przypadku zawarcia umowy o partnerstwie publiczno-prywatnym opłaty, o których
mowa w art. 13 ust. 1 pkt 1, art. 13 ust. 2, oraz opłatę dodatkową, o której
mowa w art. 13f ust. 1, może pobierać partner prywatny.

Art.13ha
--------

1. Opłata, o której mowa w art. 13 ust. 1 pkt 3, zwana dalej "opłatą
   elektroniczną", jest pobierana za przejazd po drogach krajowych lub ich
   odcinkach, określonych w przepisach wydanych na podstawie ust. 6.
2. Opłatę elektroniczną ustala się jako iloczyn liczby kilometrów przejazdu i
   stawki tej opłaty za kilometr dla danej kategorii pojazdu.
3. Ustala się następujące kategorie pojazdów w celu określenia stawki opłaty
   elektronicznej:

   1) kategoria 1 - pojazdy samochodowe o dopuszczalnej masie całkowitej
      powyżej 3,5 tony i poniżej 12 ton;
   2) kategoria 2 - pojazdy samochodowe o dopuszczalnej masie całkowitej co
      najmniej 12 ton;
   3) kategoria 3 - autobusy.

4. Stawka opłaty elektronicznej za przejazd kilometra drogą krajową,
   niezależnie od kategorii pojazdów, o której mowa w ust. 3, nie może być
   wyższa niż 2 zł oraz nie może przekroczyć stawki tej opłaty obliczonej
   zgodnie z przepisami wydanymi na podstawie ust. 5.
5. Rada Ministrów określi, w drodze rozporządzenia, szczegółową metodę
   obliczania maksymalnej stawki opłaty elektronicznej za przejazd 1 kilometra
   drogi krajowej, uwzględniając koszty budowy drogi krajowej lub jej odcinka,
   w tym koszty finansowe, koszty utrzymania, koszty remontów, ochrony oraz
   zarządzania drogą, koszty poboru opłaty elektronicznej oraz inne koszty
   eksploatacji.
6. Rada Ministrów, w drodze rozporządzenia:

   1) określi drogi krajowe lub ich odcinki, na których pobiera się opłatę
      elektroniczną,
   2) ustali dla nich wysokość stawek opłaty elektronicznej za przejazd
      kilometra, dla danej kategorii pojazdu, w wysokości nie większej niż
      określona w ust. 4

   – mając na uwadze potrzeby utrzymania i ochrony dróg istotnych dla rozwoju
   sieci drogowej, koszty poboru opłaty elektronicznej oraz klasę drogi, na
   której jest pobierana opłata elektroniczna.
7. Rada Ministrów w rozporządzeniu, o którym mowa w ust. 6, może:

   1) zróżnicować stawki opłaty elektronicznej ze względu na liczbę osi, emisję
      spalin pojazdu samochodowego, porę dnia, kategorię dnia i porę roku,
      mając na uwadze zapewnienie potrzeb ochrony środowiska, płynności ruchu,
      ochronę dróg publicznych, optymalizację wykorzystania infrastruktury
      transportu lądowego, a także propagowanie bezpieczeństwa ruchu drogowego;
   2) wprowadzić stawki abonamentowe dla niektórych użytkowników drogi krajowej
      w wysokości nie mniejszej niż 87% stawki opłaty elektronicznej, z
      zachowaniem zasady przejrzystości i niedyskryminacji.

Art.13hb
--------

1. Opłatę elektroniczną pobiera Generalny Dyrektor Dróg Krajowych i Autostrad.
2. Opłata elektroniczna stanowi przychód Krajowego Funduszu Drogowego.
3. Minister właściwy do spraw transportu, za zgodą Rady Ministrów, może
   powierzyć przygotowanie, wdrożenie, budowę lub eksploatację systemu
   elektronicznego poboru opłat elektronicznych, w tym pobór opłaty
   elektronicznej, drogowej spółce specjalnego przeznaczenia, utworzonej w
   trybie ustawy z dnia 12 stycznia 2007 r. o drogowych spółkach specjalnego
   przeznaczenia, z zastrzeżeniem art. 13hd ust. 3.

Art.13hc
--------

1. Uiszczenie opłaty elektronicznej następuje w systemie elektronicznego poboru
   opłat, zgodnie z art. 13i.
2. Podmioty pobierające opłaty elektroniczne z wykorzystaniem systemów
   elektronicznego poboru opłat mogą umożliwić użytkownikom dróg krajowych
   uiszczanie tych opłat bez konieczności instalacji urządzenia, o którym mowa
   w art. 13i ust. 3.

Art.13hd
--------

1. Generalny Dyrektor Dróg Krajowych i Autostrad albo drogowa spółka
   specjalnego przeznaczenia, o której mowa w art. 13hb ust. 3, za zgodą
   ministra właściwego do spraw transportu i po uzgodnieniu z ministrem
   właściwym do spraw finansów publicznych, może w drodze umowy powierzyć
   budowę lub eksploatację systemu elektronicznego poboru opłat elektronicznych
   innemu podmiotowi, zwanemu dalej "operatorem".
2. Wybór operatora następuje zgodnie z ustawą z dnia 29 stycznia 2004 r. -
   Prawo zamówień publicznych (Dz. U. z 2013 r. poz. 907, z późn. zm.), ustawą
   z dnia 19 grudnia 2008 r. o partnerstwie publiczno-prywatnym, ustawą z dnia
   9 stycznia 2009 r. o koncesji na roboty budowlane lub usługi albo ustawą z
   dnia 27 października 1994 r. o autostradach płatnych oraz o Krajowym
   Funduszu Drogowym (Dz. U. z 2012 r. poz. 931, z późn. zm.).
3. Umowa, o której mowa w ust. 1, może upoważnić operatora do poboru tych
   opłat.
4. Umowa, o której mowa w ust. 1, przewiduje coroczne prawo odkupu, za
   wynagrodzeniem ustalonym na podstawie tej umowy, na rzecz Generalnego
   Dyrektora Dróg Krajowych i Autostrad lub drogowej spółki specjalnego
   przeznaczenia, systemu elektronicznego poboru opłat elektronicznych, w
   przypadku gdy system ten został sfinansowany ze środków operatora.

Art.13i
-------

1. Wprowadzane po dniu 1 stycznia 2007 r. systemy elektronicznego poboru opłat,
   o których mowa w art. 13 ust. 1 pkt 3 i ust. 2, oraz opłaty za przejazd
   autostradą, o których mowa w ustawie z dnia 27 października 1994 r. o
   autostradach płatnych oraz o Krajowym Funduszu Drogowym, powinny
   wykorzystywać co najmniej jedną z następujących technologii:

   1) lokalizację satelitarną;
   2) system łączności ruchomej opartej na standardzie GSM-GPRS, zgodny z
      normami państw członkowskich Unii Europejskiej wdrażających normę GSM TS
      03.60/23.060;
   3) system radiowy do obsługi transportu i ruchu drogowego pracujący w paśmie
      częstotliwości 5,8 GHz.

2. Podmioty pobierające opłaty z wykorzystaniem systemów elektronicznego poboru
   opłat powinny umożliwiać świadczenie europejskiej usługi opłaty
   elektronicznej, począwszy od daty określonej w rozporządzeniu wydanym na
   podstawie ust. 6, jednak nie wcześniej niż od dnia:

   1) 1 stycznia 2009 r. - dla pojazdów o dopuszczalnej masie całkowitej nie
      mniejszej niż 3,5 tony oraz pojazdów samochodowych przewożących więcej
      niż 9 pasażerów;
   2) 1 stycznia 2011 r. - dla pozostałych pojazdów samochodowych.

3. Podmioty pobierające opłaty z wykorzystaniem systemów elektronicznego poboru
   opłat powinny oferować urządzenia na potrzeby pobierania tych opłat do
   instalacji w pojazdach samochodowych w rozumieniu art. 2 pkt 33 ustawy z
   dnia 20 czerwca 1997 r. - Prawo o ruchu drogowym.
4. Urządzenia, o których mowa w ust. 3, powinny być interoperacyjne i zdolne do
   komunikowania się między systemami elektronicznego poboru opłat na
   terytorium Rzeczypospolitej Polskiej oraz wszystkimi systemami używanymi na
   terytorium pozostałych państw członkowskich Unii Europejskiej.

4. a) Kierujący pojazdem samochodowym wyposażonym w urządzenie, o którym mowa w
   ust. 3, jest obowiązany do wprowadzenia do urządzenia prawidłowych danych o
   kategorii pojazdu, o której mowa w art. 13ha ust. 3.

4. b) Właściciel pojazdu samochodowego oraz jego posiadacz są obowiązani do
   używania urządzenia, o którym mowa w ust. 3, zgodnie z jego przeznaczeniem.
5. Urządzenia, o których mowa w ust. 3, mogą być również wykorzystywane do
   innych celów w transporcie drogowym, pod warunkiem że nie prowadzi to do
   dodatkowych obciążeń użytkowników lub stworzenia dyskryminacji między
   nimi. Urządzenia mogą być połączone z zainstalowanym w pojeździe
   samochodowym tachografem.
6. (uchylony)

Art.13j
-------

Przepisów art. 13i nie stosuje się do:

1) systemów opłat, których poboru nie dokonuje się środkami elektronicznymi;
2) systemów elektronicznych opłat, które nie wymagają instalowania w pojazdach
   samochodowych urządzeń na potrzeby poboru opłat;
3) systemów opłat wprowadzanych na drogach powiatowych i gminnych, w
   odniesieniu do których koszty dostosowania do wymagań wymienionych w
   art. 13i są niewspółmiernie wysokie do korzyści.

Art.13k
-------

1. Za naruszenie obowiązku uiszczenia opłaty elektronicznej, o którym mowa w
   art. 13 ust. 1 pkt 3, wymierza się karę pieniężną w wysokości:

   1) 500 zł - w przypadku zespołu pojazdów o łącznej dopuszczalnej masie
      całkowitej powyżej 3,5 tony złożonego z samochodu osobowego o
      dopuszczalnej masie całkowitej nieprzekraczającej 3,5 tony oraz
      przyczepy;
   2) 1500 zł - w pozostałych przypadkach.

2. Za naruszenie obowiązku, o którym mowa w art. 13i:

   1) ust. 4a,
   2) ust. 4b, jeśli skutkuje ono uiszczeniem opłaty w niepełnej wysokości

   – wymierza się karę pieniężną.
3. Karę pieniężną, o której mowa w ust. 2, wymierza się w wysokości:

   1) 250 zł - w przypadku zespołu pojazdów o łącznej dopuszczalnej masie
      całkowitej powyżej 3,5 tony złożonego z samochodu osobowego o
      dopuszczalnej masie całkowitej nieprzekraczającej 3,5 tony oraz
      przyczepy;
   2) 750 zł - w pozostałych przypadkach.

4. Kary pieniężne, o których mowa w ust. 1 oraz ust. 2 pkt 2, wymierza się
   właścicielowi pojazdu samochodowego, o którym mowa w art. 13 ust. 1 pkt 3, a
   jeśli pojazdem jest zespół pojazdów to właścicielowi pojazdu złączonego z
   przyczepą lub naczepą. Jeżeli właściciel nie jest posiadaczem pojazdu to
   kary nakłada się na podmiot, na rzecz którego przeniesiono posiadanie tego
   pojazdu.
5. Karę pieniężną, o której mowa w ust. 2 pkt 1, wymierza się kierującemu
   pojazdem samochodowym, o którym mowa w art. 13 ust. 1 pkt 3.
6. Na podmiot, o którym mowa w:

   1) ust. 4, nie może zostać nałożona więcej niż jedna kara pieniężna za
      poszczególne naruszenia, o których mowa w ust. 1 lub ust. 2 pkt 2,
      dotyczące danego pojazdu samochodowego,
   2) ust. 5, nie może zostać nałożona więcej niż jedna kara pieniężna za
      naruszenie, o którym mowa w ust. 2 pkt 1

   – stwierdzone w trakcie jednej doby.
7. Za dobę, w rozumieniu ust. 6, uznaje się okres od godziny 000do godziny
   2400w danym dniu.
8. Kary pieniężnej, o której mowa w ust. 1, nie nakłada się jeśli naruszenie
   obowiązku uiszczenia opłaty elektronicznej, o którym mowa w art. 13 ust. 1
   pkt 3, jest wynikiem naruszenia przez kierującego pojazdem samochodowym
   obowiązku, o którym mowa w art. 13i ust. 4a.
9. Kary pieniężne, o których mowa w ust. 1 i 2, nakłada się w drodze decyzji
   administracyjnej.

Art.13l
-------

1. Do kontroli prawidłowości uiszczenia opłaty elektronicznej, w tym kontroli
   używanego w pojeździe urządzenia, o którym mowa w art. 13i ust. 3, jeżeli
   jest ono wymagane, oraz nałożenia i pobierania kar pieniężnych, o których
   mowa w art. 13k, uprawniony jest Główny Inspektor Transportu Drogowego.
2. Minister właściwy do spraw transportu określi, w drodze rozporządzenia,
   szczegółowy tryb, sposób i zakres kontroli, o której mowa w ust. 1,
   uwzględniając technologię wykorzystaną w systemie elektronicznego poboru
   opłat elektronicznych.

Art.13m
-------

1. Kary pieniężne, o których mowa w art. 13k ust. 1 i 2, uiszcza się na
   właściwy rachunek bankowy w terminie 21 dni od dnia, w którym decyzja w
   sprawie nałożenia kary stała się ostateczna. Koszty związane z uiszczeniem
   kary pokrywa obowiązany podmiot.
2. W przypadku gdy zostanie stwierdzona okoliczność uzasadniająca nałożenie
   kary pieniężnej, o której mowa w art. 13k ust. 1 lub 2, na zagraniczny
   podmiot mający siedzibę albo miejsce zamieszkania w państwie, z którym
   Rzeczpospolita Polska nie jest związana umową lub porozumieniem o współpracy
   we wzajemnym dochodzeniu należności bądź możliwość egzekucji należności nie
   wynika wprost z przepisów międzynarodowych oraz przepisów tego państwa,
   osoba przeprowadzająca kontrolę pobiera kaucję w wysokości odpowiadającej
   przewidywanej karze pieniężnej.
3. Kaucję pobiera się w formie:

   1) gotówkowej, za pokwitowaniem na druku ścisłego zarachowania,
   2) bezgotówkowej, za pomocą karty płatniczej, przy czym koszty związane z
      autoryzacją transakcji i przekazem środków ponosi obowiązany podmiot, lub
   3) przelewu na wyodrębniony rachunek bankowy organu prowadzącego
      postępowanie administracyjne w sprawie o nałożenie kary pieniężnej, o
      której mowa w art. 13k ust. 1 lub 2, przy czym koszty przelewu ponosi
      obowiązany podmiot.

4. Kaucja przechowywana jest na nieoprocentowanym rachunku bankowym, o którym
   mowa w ust. 3 pkt 3.
5. Organ prowadzący postępowanie administracyjne w sprawie o nałożenie kary
   pieniężnej, o której mowa w art. 13k ust. 1 lub 2, przekazuje kaucję:

   1) na rachunek bankowy, o którym mowa w ust. 1 - w terminie 7 dni od dnia, w
      którym decyzja o nałożeniu kary stała się ostateczna;
   2) na rachunek bankowy podmiotu, który ją wpłacił - w terminie 7 dni od dnia
      doręczenia decyzji o uchyleniu lub stwierdzeniu nieważności decyzji o
      nałożeniu kary pieniężnej.

6. W przypadku gdy wysokość nałożonej kary pieniężnej, o której mowa w art. 13k
   ust. 1 lub 2, jest mniejsza od wysokości pobranej kaucji, do powstałej
   różnicy stosuje się odpowiednio przepis ust. 5 pkt 2.
7. Jeśli w sytuacji, o której mowa w ust. 2, nie pobrano kaucji, osoba
   przeprowadzająca kontrolę kieruje lub usuwa pojazd, na koszt podmiotu
   obowiązanego, na najbliższy parking strzeżony, o którym mowa w art. 130a
   ust. 5c ustawy z dnia 20 czerwca 1997 r. - Prawo o ruchu drogowym.
8. W zakresie postępowania w związku z usuwaniem pojazdu stosuje się
   odpowiednio przepisy art. 130a ustawy z dnia 20 czerwca 1997 r. - Prawo o
   ruchu drogowym.
9. Zwrot pojazdu z parkingu następuje po przekazaniu kaucji przez podmiot, o
   którym mowa w ust. 2, na zasadach określonych w ust. 3.
10. Jeżeli kara pieniężna, o której mowa w art. 13k ust. 1 lub 2, nie zostanie
    uiszczona lub pojazd nie zostanie odebrany z parkingu w ciągu 30 dni od
    dnia, w którym decyzja w sprawie nałożenia kary stała się ostateczna,
    stosuje się odpowiednio przepisy działu II rozdziału 6 ustawy z dnia 17
    czerwca 1966 r. o postępowaniu egzekucyjnym w administracji (Dz. U. z
    2014 r. poz. 1619, z późn. zm.) dotyczące egzekucji należności pieniężnych
    z ruchomości.

Art.13n
-------

Nie wszczyna się postępowania w sprawie nałożenia kar pieniężnych, o których
mowa w art. 13k ust. 1 i 2, jeżeli od dnia popełnienia naruszenia upłynęło 6
miesięcy.

Art.14
------

(uchylony)

Art.15
------

(uchylony)

Art.16
------

1. Budowa lub przebudowa dróg publicznych spowodowana inwestycją niedrogową
   należy do inwestora tego przedsięwzięcia.
2. Szczegółowe warunki budowy lub przebudowy dróg, o których mowa w ust. 1,
   określa umowa między zarządcą drogi a inwestorem inwestycji niedrogowej.
3. W przypadku, w którym inwestycją niedrogową jest kanał technologiczny,
   umowa, o której mowa w ust. 2, może przewidywać przekazanie zarządcy drogi
   kanału technologicznego, na warunkach uzgodnionych w umowie, z możliwością
   ustanowienia na rzecz przekazującego inwestora prawa do korzystania z części
   kanału. W takim przypadku do ustanowienia prawa na rzecz przekazującego
   inwestora postanowień art. 39 ust. 7-7f nie stosuje się.
4. Przekazywany kanał technologiczny, o którym mowa w ust. 3, powinien
   odpowiadać warunkom technicznym określonym w odrębnych przepisach, jak
   również warunkom określonym w decyzji, o której mowa w art. 39 ust. 3.

Rozdział 1a — (uchylony)
************************


Rozdział 2 — Administracja drogowa
**********************************


Art.17
------

1. Do zakresu działania ministra właściwego do spraw transportu należy:

   1) określanie kierunków rozwoju sieci drogowej;
   2) wydawanie przepisów techniczno-budowlanych i eksploatacyjnych dotyczących
      dróg i drogowych obiektów inżynierskich;
   3) koordynacja działań w zakresie przygotowania dróg na potrzeby obrony
      państwa;
   4) koordynacja działań na rzecz rozwiązywania problemów klęsk żywiołowych w
      zakresie dróg publicznych;
   5) (uchylony)
   6) sprawowanie nadzoru nad Generalnym Dyrektorem Dróg Krajowych i Autostrad.

2. Minister właściwy do spraw transportu w porozumieniu z Ministrem Obrony
   Narodowej określa, w drodze rozporządzenia, sposób koordynacji działań, o
   których mowa w ust. 1 pkt 3, uwzględniając w szczególności konieczność
   zapewnienia sprawnego współdziałania właściwych organów i instytucji w tym
   zakresie.

Art.18
------

1. Centralnym organem administracji rządowej właściwym w sprawach dróg
   krajowych jest Generalny Dyrektor Dróg Krajowych i Autostrad, do którego
   należy:

   1) wykonywanie zadań zarządcy dróg krajowych;
   2) realizacja budżetu państwa w zakresie dróg krajowych.

2. Do Generalnego Dyrektora Dróg Krajowych i Autostrad należy również:

   1) współudział w realizacji polityki transportowej w zakresie dróg;

   1) a) gromadzenie danych i sporządzanie informacji o sieci dróg publicznych;
   2) nadzór nad przygotowaniem infrastruktury drogowej na potrzeby obrony
      państwa;
   3) wydawanie zezwoleń na przejazd pojazdów nienormatywnych;
   4) współpraca z administracjami drogowymi innych państw i organizacjami
      międzynarodowymi;
   5) współpraca z organami samorządu terytorialnego w zakresie rozbudowy i
      utrzymania infrastruktury drogowej;
   6) zarządzanie ruchem na drogach krajowych;

   6) a) ochrona zabytków drogownictwa;
   7) wykonywanie zadań związanych z przygotowywaniem i koordynowaniem budowy i
      eksploatacji albo wyłącznie eksploatacji, autostrad płatnych, w tym:

      a) prowadzenie prac studialnych dotyczących autostrad płatnych,
         przygotowywanie dokumentów wymaganych w postępowaniu w sprawie oceny
         oddziaływania na środowisko - na etapie wydania decyzji o ustaleniu
         lokalizacji autostrady, o którym mowa w przepisach o ochronie
         środowiska,
      b) współpraca z organami właściwymi w sprawach zagospodarowania
         przestrzennego, obrony narodowej, geodezji i gospodarki gruntami,
         ewidencji gruntów i budynków, scalania i wymiany gruntów, melioracji
         wodnych, ochrony gruntów rolnych i leśnych, ochrony środowiska oraz
         ochrony zabytków,
      c) nabywanie, w imieniu i na rzecz Skarbu Państwa, nieruchomości pod
         autostrady i gospodarowanie nimi w ramach posiadanego prawa do
         nieruchomości,
      d) opracowywanie projektów kryteriów oceny ofert w postępowaniu
         przetargowym i przeprowadzanie postępowań przetargowych,
      e) (uchylona)
      f) uzgadnianie projektu budowlanego autostrady lub jej odcinka w zakresie
         zgodności z przepisami techniczno-budowlanymi dotyczącymi autostrad
         płatnych,
      g) kontrola budowy i eksploatacji autostrady w zakresie przestrzegania
         warunków umowy o budowę i eksploatację albo wyłącznie eksploatację
         autostrady,
      h) wykonywanie innych zadań, w sprawach dotyczących autostrad,
         określonych przez ministra właściwego do spraw transportu;

   8) pobieranie opłat za przejazd zgodnie z przepisami o autostradach płatnych
      oraz o Krajowym Funduszu Drogowym;

   8) a) pobieranie opłaty elektronicznej zgodnie z art. 13hb ust. 1;
   9) podejmowanie działań mających na celu wprowadzenie systemów
      elektronicznego poboru opłat i szerokiego zastosowania tych systemów, w
      zakresie określonym w ustawie;
   10) wykonywanie zadań wynikających z ustawy z dnia 12 stycznia 2007 r. o
       drogowych spółkach specjalnego przeznaczenia oraz umowy zawartej w
       trybie art. 6 ust. 1 tej ustawy.

3. Generalnego Dyrektora Dróg Krajowych i Autostrad powołuje Prezes Rady
   Ministrów, spośród osób wyłonionych w drodze otwartego i konkurencyjnego
   naboru, na wniosek ministra właściwego do spraw transportu. Prezes Rady
   Ministrów odwołuje Generalnego Dyrektora Dróg Krajowych i Autostrad.
4. Minister właściwy do spraw transportu na wniosek Generalnego Dyrektora Dróg
   Krajowych i Autostrad powołuje zastępców Generalnego Dyrektora Dróg
   Krajowych i Autostrad spośród osób wyłonionych w drodze otwartego i
   konkurencyjnego naboru. Minister właściwy do spraw transportu odwołuje
   zastępców Generalnego Dyrektora Dróg Krajowych i Autostrad.
5. Stanowisko Generalnego Dyrektora Dróg Krajowych i Autostrad może zajmować
   osoba, która:

   1) posiada tytuł zawodowy magistra lub równorzędny;
   2) jest obywatelem polskim;
   3) korzysta z pełni praw publicznych;
   4) nie była skazana prawomocnym wyrokiem za umyślne przestępstwo lub umyślne
      przestępstwo skarbowe;
   5) posiada kompetencje kierownicze;
   6) posiada co najmniej 6-letni staż pracy, w tym co najmniej 3-letni staż
      pracy na stanowisku kierowniczym;
   7) posiada wykształcenie i wiedzę z zakresu spraw należących do właściwości
      Generalnego Dyrektora Dróg Krajowych i Autostrad.

6. Informację o naborze na stanowisko Generalnego Dyrektora Dróg Krajowych i
   Autostrad ogłasza się przez umieszczenie ogłoszenia w miejscu powszechnie
   dostępnym w siedzibie urzędu oraz w Biuletynie Informacji Publicznej urzędu
   i Biuletynie Informacji Publicznej Kancelarii Prezesa Rady
   Ministrów. Ogłoszenie powinno zawierać:

   1) nazwę i adres urzędu;
   2) określenie stanowiska;
   3) wymagania związane ze stanowiskiem wynikające z przepisów prawa;
   4) zakres zadań wykonywanych na stanowisku;
   5) wskazanie wymaganych dokumentów;
   6) termin i miejsce składania dokumentów;
   7) informację o metodach i technikach naboru.

7. Termin, o którym mowa w ust. 6 pkt 6, nie może być krótszy niż 10 dni od
   dnia opublikowania ogłoszenia w Biuletynie Informacji Publicznej Kancelarii
   Prezesa Rady Ministrów.
8. Nabór na stanowisko Generalnego Dyrektora Dróg Krajowych i Autostrad
   przeprowadza zespół, powołany przez ministra właściwego do spraw transportu,
   liczący co najmniej 3 osoby, których wiedza i doświadczenie dają rękojmię
   wyłonienia najlepszych kandydatów. W toku naboru ocenia się doświadczenie
   zawodowe kandydata, wiedzę niezbędną do wykonywania zadań na stanowisku, na
   które jest przeprowadzany nabór, oraz kompetencje kierownicze.
9. Ocena wiedzy i kompetencji kierowniczych, o których mowa w ust. 8, może być
   dokonana na zlecenie zespołu przez osobę niebędącą członkiem zespołu, która
   posiada odpowiednie kwalifikacje do dokonania tej oceny.
10. Członek zespołu oraz osoba, o której mowa w ust. 9, mają obowiązek
    zachowania w tajemnicy informacji dotyczących osób ubiegających się o
    stanowisko, uzyskanych w trakcie naboru.
11. W toku naboru zespół wyłania nie więcej niż 3 kandydatów, których
    przedstawia ministrowi właściwemu do spraw transportu.
12. przeprowadzonego naboru zespół sporządza protokół zawierający:

    1) nazwę i adres urzędu;
    2) określenie stanowiska, na które był prowadzony nabór, oraz liczbę
       kandydatów;
    3) imiona, nazwiska i adresy nie więcej niż 3 najlepszych kandydatów
       uszeregowanych według poziomu spełniania przez nich wymagań określonych
       w ogłoszeniu o naborze;
    4) informację o zastosowanych metodach i technikach naboru;
    5) uzasadnienie dokonanego wyboru albo powody niewyłonienia kandydata;
    6) skład zespołu.

13. Wynik naboru ogłasza się niezwłocznie przez umieszczenie informacji w
    Biuletynie Informacji Publicznej urzędu i Biuletynie Informacji Publicznej
    Kancelarii Prezesa Rady Ministrów. Informacja o wyniku naboru zawiera:

    1) nazwę i adres urzędu;
    2) określenie stanowiska, na które był prowadzony nabór;
    3) imiona, nazwiska wybranych kandydatów oraz ich miejsca zamieszkania w
       rozumieniu przepisów Kodeksu cywilnego albo informację o niewyłonieniu
       kandydata.

14. Umieszczenie w Biuletynie Informacji Publicznej Kancelarii Prezesa Rady
    Ministrów ogłoszenia o naborze oraz o wyniku tego naboru jest bezpłatne.
15. Zespół przeprowadzający nabór na stanowiska, o których mowa w ust. 4,
    powołuje Generalny Dyrektor Dróg Krajowych i Autostrad.
16. Do sposobu przeprowadzania naboru na stanowiska, o których mowa w ust. 4,
    stosuje się odpowiednio ust. 5-14.

Art.18a
-------

1. Generalny Dyrektor Dróg Krajowych i Autostrad realizuje swoje zadania przy
   pomocy Generalnej Dyrekcji Dróg Krajowych i Autostrad. Generalna Dyrekcja
   Dróg Krajowych i Autostrad wykonuje również zadania zarządu dróg krajowych.
2. W skład Generalnej Dyrekcji Dróg Krajowych i Autostrad wchodzą, z
   zastrzeżeniem ust. 3a, oddziały w województwach.
3. Obszar działania oddziału pokrywa się, z zastrzeżeniem ust. 3a, z obszarem
   województwa.

3. a) Minister właściwy do spraw transportu, na wniosek Generalnego Dyrektora
   Dróg Krajowych i Autostrad, w drodze zarządzenia, może tworzyć oddziały
   regionalne realizujące zadania Generalnego Dyrektora Dróg Krajowych i
   Autostrad w zakresie poszczególnych autostrad i dróg ekspresowych, przy czym
   oddziały te prowadzą działalność na obszarze większym niż jedno województwo.
4. Generalny Dyrektor Dróg Krajowych i Autostrad powołuje i odwołuje dyrektorów
   oddziałów Generalnej Dyrekcji Dróg Krajowych i Autostrad.
5. Generalny Dyrektor Dróg Krajowych i Autostrad może upoważnić pracowników
   Generalnej Dyrekcji Dróg Krajowych i Autostrad do załatwiania określonych
   spraw w jego imieniu w ustalonym zakresie, w szczególności do wydawania
   decyzji administracyjnych.
6. Minister właściwy do spraw transportu, w drodze zarządzenia, nadaje
   Generalnej Dyrekcji Dróg Krajowych i Autostrad statut, określający jej
   wewnętrzną organizację.
7. Minister właściwy do spraw transportu określi, w drodze rozporządzenia, tryb
   sporządzania informacji oraz gromadzenia i udostępniania danych o sieci dróg
   publicznych, obiektach mostowych, tunelach oraz promach, uwzględniając
   potrzebę zapewnienia ich jednolitości i kompletności.
8. Generalna Dyrekcja Dróg Krajowych i Autostrad otrzymuje środki, ustalane
   corocznie w ustawie budżetowej, na związane z budową autostrad prace
   studialne i dokumentacyjne, przejęcie nieruchomości i gospodarowanie nimi,
   odszkodowania, należności z tytułu ochrony gruntów leśnych, prace scaleniowe
   i wymienne, przeprowadzanie ratowniczych badań archeologicznych i badań
   ekologicznych oraz opracowywanie ich wyników.

Art.19
------

1. Organ administracji rządowej lub jednostki samorządu terytorialnego, do
   którego właściwości należą sprawy z zakresu planowania, budowy, przebudowy,
   remontu, utrzymania i ochrony dróg, jest zarządcą drogi.
2. Zarządcami dróg, z zastrzeżeniem ust. 3, 5 i 8, są dla dróg:

   1) krajowych - Generalny Dyrektor Dróg Krajowych i Autostrad;
   2) wojewódzkich - zarząd województwa;
   3) powiatowych - zarząd powiatu;
   4) gminnych - wójt (burmistrz, prezydent miasta).

3. Generalny Dyrektor Dróg Krajowych i Autostrad jest zarządcą autostrady
   wybudowanej na zasadach określonych w ustawie do czasu przekazania jej, w
   drodze porozumienia, spółce, z którą zawarto umowę o budowę i eksploatację
   albo wyłącznie eksploatację autostrady. Spółka pełni funkcję zarządcy
   autostrady płatnej na warunkach określonych w umowie o budowę i eksploatację
   albo wyłącznie eksploatację autostrady, z wyjątkiem zadań, o których mowa w
   art. 20 pkt 1, 8, 17 i 20, które wykonuje Generalny Dyrektor Dróg Krajowych
   i Autostrad.

3. a) (uchylony)
4. Zarządzanie drogami publicznymi może być przekazywane między zarządcami w
   trybie porozumienia, regulującego w szczególności wzajemne rozliczenia
   finansowe.
5. W granicach miast na prawach powiatu zarządcą wszystkich dróg publicznych, z
   wyjątkiem autostrad i dróg ekspresowych, jest prezydent miasta.
6. (uchylony)
7. W przypadku zawarcia umowy o partnerstwie publiczno-prywatnym zadania
   zarządcy, o których mowa w art. 20 pkt 3-5, 7, 11-13 oraz 15 i 16, może
   wykonywać partner prywatny.
8. Drogowa spółka specjalnego przeznaczenia pełni funkcję zarządcy drogi
   krajowej na zasadach określonych w ustawie z dnia 12 stycznia 2007 r. o
   drogowych spółkach specjalnego przeznaczenia oraz umowie, o której mowa w
   art. 6 ust. 1 tej ustawy.

Art.20
------

Do zarządcy drogi należy w szczególności:

1) opracowywanie projektów planów rozwoju sieci drogowej oraz bieżące
   informowanie o tych planach organów właściwych do sporządzania miejscowych
   planów zagospodarowania przestrzennego;
2) opracowywanie projektów planów finansowania budowy, przebudowy, remontu,
   utrzymania i ochrony dróg oraz drogowych obiektów inżynierskich;
3) pełnienie funkcji inwestora;
4) utrzymanie nawierzchni drogi, chodników, drogowych obiektów inżynierskich,
   urządzeń zabezpieczających ruch i innych urządzeń związanych z drogą, z
   wyjątkiem części pasa drogowego, o których mowa w art. 20f pkt 2.
5) realizacja zadań w zakresie inżynierii ruchu;
6) przygotowanie infrastruktury drogowej dla potrzeb obronnych oraz wykonywanie
   innych zadań na rzecz obronności kraju;
7) koordynacja robót w pasie drogowym;
8) wydawanie zezwoleń na zajęcie pasa drogowego i zjazdy z dróg oraz pobieranie
   opłat i kar pieniężnych;
9) prowadzenie ewidencji dróg, obiektów mostowych, tuneli, przepustów i promów
   oraz udostępnianie ich na żądanie uprawnionym organom;

9) a) sporządzanie informacji o drogach publicznych oraz przekazywanie ich
   Generalnemu Dyrektorowi Dróg Krajowych i Autostrad;
10) przeprowadzanie okresowych kontroli stanu dróg i drogowych obiektów
    inżynierskich oraz przepraw promowych, ze szczególnym uwzględnieniem ich
    wpływu na stan bezpieczeństwa ruchu drogowego, w tym weryfikację cech i
    wskazanie usterek, które wymagają prac konserwacyjnych lub naprawczych ze
    względu na bezpieczeństwo ruchu drogowego;

10) a) badanie wpływu robót drogowych na bezpieczeństwo ruchu drogowego;
11) wykonywanie robót interwencyjnych, robót utrzymaniowych i
    zabezpieczających;
12) przeciwdziałanie niszczeniu dróg przez ich użytkowników;
13) przeciwdziałanie niekorzystnym przeobrażeniom środowiska mogącym powstać
    lub powstającym w następstwie budowy lub utrzymania dróg;
14) wprowadzanie ograniczeń lub zamykanie dróg i drogowych obiektów
    inżynierskich dla ruchu oraz wyznaczanie objazdów drogami różnej kategorii,
    gdy występuje bezpośrednie zagrożenie bezpieczeństwa osób lub mienia;
15) dokonywanie okresowych pomiarów ruchu drogowego;
16) utrzymywanie zieleni przydrożnej, w tym sadzenie i usuwanie drzew oraz
    krzewów;
17) nabywanie nieruchomości pod pasy drogowe dróg publicznych i gospodarowanie
    nimi w ramach posiadanego prawa do tych nieruchomości;
18) nabywanie nieruchomości innych niż wymienione w pkt 17 na potrzeby
    zarządzania drogami i gospodarowanie nimi w ramach posiadanego do nich
    prawa;
19) zarządzanie i utrzymywanie kanałów technologicznych i pobieranie opłat, o
    których mowa w art. 39 ust. 7;
20) zarządzanie bezpieczeństwem dróg w transeuropejskiej sieci drogowej.

Art.20a
-------

Do zarządcy drogi, o którym mowa w art. 19 ust. 2 pkt 1 i 2, należy ponadto
budowa, przebudowa, remont i utrzymanie:

1) parkingów przeznaczonych dla postoju pojazdów wykonujących przewozy drogowe,
   wynikającego z konieczności przestrzegania przepisów o czasie prowadzenia
   pojazdów oraz przepisów o ograniczeniach i zakazach ruchu drogowego;
2) miejsc wykonywania kontroli ruchu i transportu drogowego, przeznaczonych w
   szczególności do ważenia pojazdów.

Art.20b
-------

1. Do zarządcy drogi należy ponadto instalacja w pasie drogowym stacjonarnych
   urządzeń rejestrujących, o których mowa w art. 2 pkt 59 ustawy z dnia 20
   czerwca 1997 r. - Prawo o ruchu drogowym, obudów na te urządzenia, ich
   usuwanie oraz utrzymanie zewnętrznej infrastruktury dla zainstalowanych
   urządzeń.
2. Zarządcy dróg instalują lub usuwają stacjonarne urządzenia rejestrujące lub
   obudowy na te urządzenia na wniosek Głównego Inspektora Transportu Drogowego
   lub z inicjatywy własnej, za zgodą Głównego Inspektora Transportu Drogowego.
3. Zadania, o których mowa w ust. 1, są finansowane przez zarządcę drogi.
4. W przypadku dróg zarządzanych przez Generalnego Dyrektora Dróg Krajowych i
   Autostrad obowiązki, o których mowa w ust. 1-3, wykonuje Główny Inspektor
   Transportu Drogowego.
5. Przepisy ust. 1-3 o zarządcy drogi stosuje się odpowiednio do straży
   gminnych (miejskich) w przypadku urządzeń rejestrujących eksploatowanych
   przez te straże.
6. W pasie drogowym dróg publicznych zabrania się instalowania, usuwania, a
   także eksploatowania (bieżącej obsługi) urządzeń rejestrujących lub obudów
   na te urządzenia przez podmioty inne niż zarządcy dróg, Policja, Inspekcja
   Transportu Drogowego oraz straże gminne (miejskie).
7. Zarządcy dróg, Policja, Inspekcja Transportu Drogowego oraz straże gminne
   (miejskie) mogą zlecać w swoim imieniu innym podmiotom: instalację,
   usunięcie a także obsługę techniczną obejmującą naprawę, remont i wymianę
   urządzeń rejestrujących oraz obudów na te urządzenia.

Art.20c
-------

1. Zapewnienie funkcjonowania stacjonarnych urządzeń rejestrujących oraz obudów
   na te urządzenia, polegające w szczególności na: zakupie, utrzymaniu i
   naprawie oraz wynikające z ich bieżącej eksploatacji i obsługi w tym również
   import danych zarejestrowanych przez te urządzenia oraz montaż urządzeń w
   zainstalowanych obudowach, należy odpowiednio do:

   1) Inspekcji Transportu Drogowego - w przypadku urządzeń zainstalowanych
      przez Inspekcję Transportu Drogowego lub zarządcę drogi działającego na
      wniosek Głównego Inspektora Transportu Drogowego oraz w przypadku obudów
      urządzeń rejestrujących należących do Policji;
   2) jednostki samorządu terytorialnego lub straży gminnej (miejskiej) - w
      pozostałych przypadkach.

2. Zadania, o których mowa w ust. 1, są finansowane odpowiednio przez:

   1) Inspekcję Transportu Drogowego z budżetu państwa z części, której
      dysponentem jest minister właściwy do spraw transportu - w przypadku
      urządzeń określonych w ust. 1 pkt 1;
   2) właściwe jednostki samorządu terytorialnego - w przypadku urządzeń
      określonych w ust. 1 pkt 2.

Art.20d
-------

1. Dochody uzyskane z grzywien nałożonych za naruszenia przepisów ruchu
   drogowego ujawnione za pomocą urządzeń rejestrujących, jednostki samorządu
   terytorialnego przeznaczają w całości na finansowanie:

   1) zadań inwestycyjnych, modernizacyjnych lub remontowych związanych z
      siecią drogową;
   2) utrzymania i funkcjonowania infrastruktury oraz urządzeń drogowych, w tym
      na budowę, przebudowę, remont, utrzymanie i ochronę dróg oraz drogowych
      obiektów inżynierskich;
   3) poprawy bezpieczeństwa ruchu drogowego, w tym popularyzację przepisów
      ruchu drogowego, działalność edukacyjną oraz współpracę w tym zakresie z
      właściwymi organizacjami społecznymi i instytucjami pozarządowymi.

2. Środki uzyskane z grzywien nałożonych przez Inspekcję Transportu Drogowego
   za naruszenia przepisów ruchu drogowego ujawnione za pomocą urządzeń
   rejestrujących, są przekazywane w terminie pierwszych dwóch dni roboczych po
   zakończeniu tygodnia, w którym wpłynęły, na rachunek Krajowego Funduszu
   Drogowego z przeznaczeniem na finansowanie:

   1) zadań inwestycyjnych związanych z poprawą bezpieczeństwa ruchu drogowego
      na drogach krajowych;
   2) budowy lub przebudowy dróg krajowych.

Art.20e
-------

Przepisów art. 40 ust. 1 i 3 nie stosuje się do zajęcia pasa drogowego przez
Inspekcję Transportu Drogowego w związku z wykonywaniem zadań określonych w
art. 20b ust. 4 oraz art. 20c ust. 1 pkt 1.

Art.20f
-------

Zarządca drogi, o którym mowa w art. 19 ust. 2, jest obowiązany:

1) uwzględniać uchwały rady gminy, w których dla zaspokojenia potrzeb
   mieszkańców wskazane zostaną wstępne miejsca lokalizacji nowych przystanków
   komunikacyjnych; o ostatecznej lokalizacji takiego przystanku decyduje
   zarządca drogi, uwzględniając charakter drogi oraz warunki bezpieczeństwa
   ruchu drogowego;
2) udostępnić nieodpłatnie gminie na jej wniosek część pasa drogowego w celu
   budowy, przebudowy i remontu wiat przystankowych lub innych urządzeń
   służących do obsługi podróżnych.

Art.21
------

1. Zarządca drogi, o którym mowa w art. 19 ust. 2 pkt 2-4 i ust. 5, może
   wykonywać swoje obowiązki przy pomocy jednostki organizacyjnej będącej
   zarządem drogi, utworzonej odpowiednio przez sejmik województwa, radę
   powiatu lub radę gminy. Jeżeli jednostka taka nie została utworzona, zadania
   zarządu drogi wykonuje zarządca.

1. a) Zarządca drogi może upoważnić pracowników odpowiednio: urzędu
   marszałkowskiego, starostwa, urzędu miasta lub gminy albo pracowników
   jednostki organizacyjnej będącej zarządem drogi, do załatwiania spraw w jego
   imieniu, w ustalonym zakresie, a w szczególności do wydawania decyzji
   administracyjnych.
2. Zarządy dróg mają prawo do:

   1) wstępu na grunty przyległe do pasa drogowego, jeżeli jest to niezbędne do
      wykonywania czynności związanych z utrzymaniem i ochroną dróg;
   2) urządzania czasowego przejazdu przez grunty przyległe do pasa drogowego w
      razie przerwy w komunikacji na drodze;
   3) ustawiania na gruntach przyległych do pasa drogowego zasłon
      przeciwśnieżnych.

3. Właścicielom lub użytkownikom gruntów, którzy ponieśli szkody w wyniku
   czynności wymienionych w ust. 2, przysługuje odszkodowanie na zasadach
   określonych w przepisach o gospodarce nieruchomościami.

Art.22
------

1. Zarząd drogi sprawuje nieodpłatny trwały zarząd gruntami w pasie drogowym.
2. Grunty, o których mowa w ust. 1, zarząd drogi może oddawać w najem,
   dzierżawę albo je użyczać, w drodze umowy, na cele związane z potrzebami
   zarządzania drogami lub potrzebami ruchu drogowego, a także na cele związane
   z potrzebami obsługi użytkowników ruchu. Zarząd drogi może pobierać z tytułu
   najmu lub dzierżawy opłaty w wysokości ustalonej w umowie. Przepisów art. 43
   ust. 2 pkt 3 i art. 85 ust. 1 ustawy z dnia 21 sierpnia 1997 r. o gospodarce
   nieruchomościami (Dz. U. z 2014 r. poz. 518, z późn. zm.) nie stosuje się.

2. a) W przypadku zawarcia umowy o partnerstwie publiczno-prywatnym partner
   prywatny może otrzymać w najem, dzierżawę albo użyczenie nieruchomości
   leżące w pasie drogowym, w celu wykonywania działalności gospodarczej.

2. b) (uchylony)
3. W przypadku nabywania gruntów przeznaczonych pod pas drogowy zarząd drogi
   może wystąpić z wnioskiem o dokonanie podziału lub scalenia i podziału
   nieruchomości, zgodnie z przepisami o gospodarce nieruchomościami lub
   przepisami o scalaniu gruntów.

Art.23
------

(wygasł)

Art.24
------

(wygasł)

Rozdział 2a — Zarządzanie tunelami położonymi w transeuropejskiej sieci drogowej
********************************************************************************


Art.24a
-------

1. Zarządzanie tunelem położonym w transeuropejskiej sieci drogowej, będącym na
   etapie projektowania, budowy lub w użytkowaniu, o długości powyżej 500 m
   należy do zarządcy drogi, zwanego dalej "zarządzającym tunelem", o ile umowy
   międzynarodowe nie stanowią inaczej.
2. Do długości tunelu, o którym mowa w ust. 1, stosuje się przepis art. 13d
   ust. 2 ustawy.
3. Do obowiązków zarządzającego tunelem należy w szczególności:

   1) sporządzanie dokumentacji bezpieczeństwa tunelu, o której mowa w art. 24d
      ust. 1, i jej aktualizowanie;
   2) sporządzanie sprawozdań z każdego pożaru oraz wypadku, w którym są ranni
      lub zabici albo naruszona została konstrukcja tunelu, który wydarzył się
      w tunelu, i przekazywanie ich w terminie 2 tygodni od wystąpienia pożaru
      lub wypadku do wojewody, urzędnika zabezpieczenia, służb ratowniczych i
      Policji;
   3) organizowanie szkoleń i ćwiczeń, o których mowa w art. 24e ust. 1 i 2,
      dla pracowników zarządzającego tunelem, służb ratowniczych i Policji we
      współpracy z urzędnikiem zabezpieczenia oraz Policją;
   4) przygotowywanie planów bezpieczeństwa określających zasady postępowania w
      razie pożaru, wypadku, awarii technicznej lub katastrofy budowlanej.

Art.24b
-------

1. Wojewoda sprawuje nadzór nad zapewnieniem bezpieczeństwa tunelu, w
   szczególności przez:

   1) wydawanie pozwolenia na użytkowanie tunelu;
   2) wyłączanie z użytkowania lub ograniczenie użytkowania tunelu, jeżeli nie
      są spełnione wymagania bezpieczeństwa;
   3) określanie warunków umożliwiających wznowienie użytkowania tunelu;
   4) przeprowadzanie regularnych badań i kontroli tunelu oraz opracowywanie
      odnośnych wymagań bezpieczeństwa;
   5) kontrolę umieszczania w siedzibie urzędnika zabezpieczenia planów
      bezpieczeństwa, o których mowa w art. 24a ust. 3 pkt 4;
   6) określanie procedury natychmiastowego zamknięcia tunelu w razie wypadku,
      w którym są ranni lub zabici albo naruszona została konstrukcja tunelu,
      katastrofy budowlanej lub awarii technicznej;
   7) kontrolę wprowadzania działań niezbędnych do zwiększenia bezpieczeństwa
      tunelu;
   8) przeprowadzanie w tunelu, co najmniej raz na 5 lat, kontroli spełniania
      wymagań bezpieczeństwa;
   9) powiadamianie uczestników ćwiczeń, o których mowa w art. 24e ust. 1 i 2,
      o ich terminie, miejscu przeprowadzenia oraz obowiązku wzięcia w nich
      udziału.

2. W przypadku stwierdzenia, na podstawie sprawozdania, o którym mowa w
   art. 24a ust. 3 pkt 2, że tunel nie spełnia wymagań bezpieczeństwa, wojewoda
   powiadamia zarządzającego tunelem i urzędnika zabezpieczenia o konieczności
   podjęcia działań zwiększających bezpieczeństwo tunelu.
3. W sytuacji, o której mowa w ust. 2, wojewoda określa warunki użytkowania
   tunelu lub ponownego otwarcia tunelu, które będą stosowane do czasu
   wprowadzenia środków zaradczych lub innych ograniczeń.
4. W sytuacji, gdy tunel nie spełnia wymagań bezpieczeństwa, a podjęte środki
   zaradcze obejmują zasadniczą zmianę konstrukcji tunelu lub sposobu jego
   użytkowania, wojewoda przeprowadza analizę ryzyka dla danego tunelu.
5. Opracowując analizę ryzyka, o której mowa w ust. 4, bierze się pod uwagę
   wszystkie czynniki projektowe, warunki ruchu, mające wpływ na
   bezpieczeństwo, charakterystykę ruchu drogowego, długość danego tunelu i
   jego geometrię, a także dobowe natężenie ruchu przejeżdżających samochodów
   ciężarowych.

Art.24c
-------

1. Dla tunelu, o którym mowa w art. 24a ust. 1, zarządzający tunelem, w
   uzgodnieniu z wojewodą, powołuje urzędnika zabezpieczenia.
2. Urzędnik zabezpieczenia jest niezależny w zakresie wykonywania swoich
   obowiązków, wynikających z ust. 3, i może wykonywać swoje zadania w
   odniesieniu do kilku tuneli położonych na obszarze działania zarządzającego
   tunelem.
3. Do obowiązków urzędnika zabezpieczenia należy w szczególności:

   1) koordynacja współpracy zarządzającego tunelem ze służbami ratowniczymi i
      Policją;
   2) udział w przygotowaniu planów bezpieczeństwa, o których mowa w art. 24a
      ust. 3 pkt 4;
   3) udział w planowaniu, wdrażaniu i ocenie działań podejmowanych w razie
      wypadku, w którym są ranni lub zabici albo naruszona została konstrukcja
      tunelu, lub w razie katastrofy budowlanej;
   4) kontrola przeszkolenia pracowników zarządzającego tunelem oraz udział w
      organizowaniu ćwiczeń, o których mowa w art. 24e ust. 1 i 2;
   5) wspólna ze służbami ratowniczymi i Policją ocena ćwiczeń, o których mowa
      w art. 24e ust. 1 i 2, sporządzanie sprawozdania zawierającego wnioski z
      przeprowadzonych ćwiczeń oraz wnioski dotyczące oceny stanu
      bezpieczeństwa tunelu;
   6) wydawanie opinii w sprawach oddawania tunelu do użytkowania, polegające w
      szczególności na opiniowaniu dokumentacji bezpieczeństwa;
   7) kontrola prawidłowości utrzymania tuneli;
   8) współdziałanie z właściwymi służbami w ocenie każdego wypadku, w którym
      są ranni lub zabici albo naruszona została konstrukcja tunelu, lub też
      katastrofy budowlanej tunelu.

Art.24d
-------

1. Zarządzający tunelem, sporządzając dokumentację bezpieczeństwa, określa
   środki zapobiegawcze i ochronne oraz reagowania na potencjalne zdarzenia,
   konieczne dla zapewnienia bezpieczeństwa uczestników ruchu drogowego w
   tunelu, uwzględniając strukturę ruchu drogowego i inne uwarunkowania
   związane z konstrukcją i otoczeniem tunelu.
2. Dokumentacja bezpieczeństwa stanowi integralną część dokumentacji tunelu,
   gromadzonej przez zarządzającego tunelem w związku z procesem inwestycyjnym
   oraz użytkowaniem tunelu.
3. Minister właściwy do spraw transportu określi, w drodze rozporządzenia,
   elementy oraz tryb postępowania z dokumentacją bezpieczeństwa, o której mowa
   w ust. 1, mając na względzie poszczególne etapy procesu inwestycyjnego oraz
   etap użytkowania tunelu.

Art.24e
-------

1. Ćwiczenia częściowe dla pracowników zarządzającego tunelem, służb
   ratowniczych i Policji organizuje się raz w roku.
2. Ćwiczenia w pełnym zakresie są przeprowadzane w każdym tunelu przynajmniej
   raz na 4 lata.
3. Urzędnik zabezpieczenia przekazuje wojewodzie sprawozdanie, o którym mowa w
   art. 24c ust. 3 pkt 5.
4. Minister właściwy do spraw wewnętrznych i minister właściwy do spraw
   transportu określą, w drodze rozporządzenia, zakres ćwiczeń dla rodzajów
   ćwiczeń, o których mowa w ust. 1 i 2, mając na uwadze potrzeby zapewnienia
   bezpieczeństwa ruchu drogowego w tunelu.

Art.24f
-------

1. Wojewoda w trybie art. 9 ustawy z dnia 7 lipca 1994 r. - Prawo budowlane, na
   wniosek zarządzającego tunelem, może udzielić odstępstwa od wymagań
   zawartych w przepisach techniczno-budowlanych, dotyczących warunków
   bezpieczeństwa w tunelu w przypadku możliwości zastosowania rozwiązań
   technicznych o wyższych parametrach bezpieczeństwa.
2. Minister właściwy do spraw transportu ocenia zasadność wniosku, a w
   przypadku pozytywnej oceny przekazuje go Komisji Europejskiej.
3. Jeżeli w terminie 3 miesięcy od dnia przekazania wniosku Komisji
   Europejskiej, Komisja nie zgłosi sprzeciwu na odstępstwo, minister właściwy
   do spraw transportu upoważnia wojewodę do udzielenia zgody na odstępstwo. W
   przypadku sprzeciwu Komisji upoważnienie do udzielenia zgody na odstępstwo
   nie może być udzielone.

Art.24g
-------

1. Zarządzający tunelem jest obowiązany przekazywać Generalnemu Dyrektorowi
   Dróg Krajowych i Autostrad sprawozdania, o których mowa w art. 24a ust. 3
   pkt 2, co 2 lata, w terminie do dnia 30 kwietnia roku następnego po upływie
   dwuletniego okresu objętego sprawozdaniem.
2. Generalny Dyrektor Dróg Krajowych i Autostrad przekazuje, co 2 lata, Komisji
   Europejskiej zbiorczą informację z otrzymanych sprawozdań, o których mowa w
   ust. 1, ze wskazaniem częstotliwości i przyczyn pożarów oraz wypadków, w
   terminie do dnia 30 września roku następnego po upływie dwuletniego okresu
   objętego sprawozdaniem.
3. Zbiorcza informacja, o której mowa w ust. 2, zawiera ocenę pożarów i
   wypadków w tunelach i informacje na temat rzeczywistej roli i skuteczności
   urządzeń i środków bezpieczeństwa.

Rozdział 2b — Zarządzanie bezpieczeństwem dróg w transeuropejskiej sieci drogowej
*********************************************************************************


Art.24h
-------

Zarządzanie bezpieczeństwem dróg w transeuropejskiej sieci drogowej polega na:

1) przeprowadzeniu:

   a) oceny wpływu planowanej drogi na bezpieczeństwo ruchu drogowego,
   b) audytu bezpieczeństwa ruchu drogowego;
2) dokonywaniu klasyfikacji odcinków dróg:

   a) ze względu na koncentrację wypadków śmiertelnych,
   b) ze względu na bezpieczeństwo sieci drogowej.

Art.24i
-------

1. Ocenę wpływu planowanej drogi na bezpieczeństwo ruchu drogowego przeprowadza
   się na etapie planowania tej drogi przed wszczęciem postępowania w sprawie
   decyzji o środowiskowych uwarunkowaniach, o której mowa w ustawie z dnia 3
   października 2008 r. o udostępnianiu informacji o środowisku i jego
   ochronie, udziale społeczeństwa w ochronie środowiska oraz o ocenach
   oddziaływania na środowisko (Dz. U. z 2013 r. poz. 1235, z późn. zm.).
2. Przy przeprowadzaniu oceny wpływu planowanej drogi na bezpieczeństwo ruchu
   drogowego uwzględnia się:

   1) liczbę zabitych w wypadkach drogowych oraz liczbę wypadków drogowych na
      drogach, z których ruch drogowy może zostać przeniesiony na planowaną
      drogę;
   2) warianty przebiegu i parametrów planowanej drogi w przypadku budowy drogi
      oraz rozkład ruchu drogowego na sieci drogowej;
   3) wpływ planowanej drogi na istniejącą sieć drogową;
   4) wpływ planowanej drogi na uczestników ruchu drogowego;
   5) natężenie ruchu drogowego i jego rodzaj;
   6) czynniki sezonowe i klimatyczne;
   7) potrzeby uczestników ruchu drogowego w zakresie bezpiecznych stref
      parkingowych;
   8) lokalną aktywność tektoniczną, sejsmiczną oraz możliwość wystąpienia
      tąpnięć górniczych.

3. Ocena wpływu planowanej drogi na bezpieczeństwo ruchu drogowego zawiera w
   szczególności:

   1) opis planowanej budowy lub przebudowy drogi;
   2) opis stanu bezpieczeństwa ruchu drogowego istniejącego oraz jego stanu w
      przypadku niezrealizowania planowanej budowy lub przebudowy drogi;
   3) przedstawienie proponowanych i możliwych rozwiązań w zakresie
      bezpieczeństwa ruchu drogowego;
   4) analizę wpływu alternatywnych rozwiązań na bezpieczeństwo ruchu
      drogowego;
   5) porównanie rozwiązań alternatywnych, w tym analizę kosztów i korzyści.

4. Wyniki oceny wpływu planowanej drogi na bezpieczeństwo ruchu drogowego
   zarządca drogi uwzględnia na kolejnych etapach projektowania budowy albo
   przebudowy drogi.

Art.24j
-------

1. Audyt bezpieczeństwa ruchu drogowego przeprowadza się:

   1) w ramach opracowywania na potrzeby decyzji o środowiskowych
      uwarunkowaniach karty informacyjnej przedsięwzięcia lub raportu o
      oddziaływaniu przedsięwzięcia na środowisko, ich ewentualnego
      uzupełniania oraz gdy w toku postępowania w sprawie wydania decyzji o
      środowiskowych uwarunkowaniach wyniknie potrzeba przedstawienia nowego
      wariantu drogi;
   2) przed wszczęciem postępowania w sprawie wydania decyzji o zezwoleniu na
      realizację inwestycji drogowej, decyzji o pozwoleniu na budowę albo przed
      zgłoszeniem wykonywania robót;
   3) przed wszczęciem postępowania w sprawie wydania decyzji o pozwoleniu na
      użytkowanie drogi lub zawiadomieniem o zakończeniu budowy lub przebudowy
      drogi;
   4) przed upływem 12 miesięcy od dnia oddania drogi do użytkowania.

2. Przy przeprowadzaniu audytu bezpieczeństwa ruchu drogowego, o którym mowa w
   ust. 1 pkt 1, uwzględnia się w szczególności:

   1) położenie geograficzne oraz warunki geograficzne, klimatyczne i
      meteorologiczne;
   2) lokalizację i rodzaj skrzyżowań;
   3) ograniczenia ruchu;
   4) funkcjonalność w ramach sieci drogowej;
   5) dopuszczalną oraz projektową prędkość pojazdu;
   6) przekrój poprzeczny, w tym liczbę i szerokość pasów ruchu;
   7) plan sytuacyjny i profil podłużny;
   8) ograniczenia widoczności;
   9) dostępność dla środków publicznego transportu zbiorowego;
   10) skrzyżowania z liniami kolejowymi;
   11) projektowane przejścia dla zwierząt i inne urządzenia ochrony
       środowiska.

3. Przy przeprowadzaniu audytu bezpieczeństwa ruchu drogowego, o którym mowa w
   ust. 1 pkt 2, uwzględnia się w szczególności:

   1) oznakowanie pionowe i poziome drogi, na podstawie projektu organizacji
      ruchu;
   2) oświetlenie drogi i skrzyżowań;
   3) urządzenia i obiekty w pasie drogowym;
   4) sposób zagospodarowania terenów przyległych do pasa drogowego, w tym
      roślinność;
   5) uczestników ruchu drogowego oraz ich potrzeby w zakresie bezpiecznych
      stref parkingowych;
   6) sposób dostosowania urządzeń bezpieczeństwa ruchu drogowego do potrzeb
      uczestników ruchu drogowego.

4. Przy przeprowadzaniu audytu bezpieczeństwa ruchu drogowego, o którym mowa w
   ust. 1 pkt 3, uwzględnia się w szczególności:

   1) bezpieczeństwo uczestników ruchu drogowego i widoczność w różnych
      warunkach pogodowych oraz porach dnia;
   2) widoczność oznakowania pionowego i poziomego drogi na podstawie wizji
      lokalnej w terenie;
   3) stan nawierzchni drogi.

5. Przy przeprowadzaniu audytu bezpieczeństwa ruchu drogowego, o którym mowa w
   ust. 1 pkt 4, uwzględnia się ocenę zachowań uczestników ruchu drogowego i
   wpływ tych zachowań na bezpieczeństwo ruchu drogowego.
6. Przy przeprowadzaniu audytu bezpieczeństwa ruchu drogowego, o którym mowa w
   ust. 3-5, uwzględnia się w razie potrzeby kryteria stosowane przy
   przeprowadzaniu audytu na etapach wcześniejszych.

Art.24k
-------

1. Audyt bezpieczeństwa ruchu drogowego przeprowadza audytor bezpieczeństwa
   ruchu drogowego lub zespół audytujący, w którego skład wchodzi co najmniej
   jeden audytor bezpieczeństwa ruchu drogowego.
2. Audyt bezpieczeństwa ruchu drogowego nie może być przeprowadzany przez
   audytora bezpieczeństwa ruchu drogowego, który wykonywał lub wykonuje
   zadania w zakresie projektowania, budowy, przebudowy, zarządzania odcinkiem
   drogi podlegającym audytowi, zarządzania ruchem lub nadzoru nad zarządzaniem
   ruchem na odcinku drogi podlegającym audytowi.
3. Audyt bezpieczeństwa ruchu drogowego nie może być przeprowadzany przez
   audytora bezpieczeństwa ruchu drogowego, którego małżonek, krewny i
   powinowaty do drugiego stopnia, osoba związana z nim z tytułu
   przysposobienia, opieki lub kurateli wykonywał lub wykonuje zadania w
   zakresie projektowania, budowy, przebudowy, zarządzania odcinkiem drogi
   podlegającym audytowi, zarządzania ruchem lub nadzoru nad zarządzaniem
   ruchem na odcinku drogi podlegającym audytowi.
4. Audyt bezpieczeństwa ruchu drogowego może przeprowadzać audytor
   bezpieczeństwa ruchu drogowego będący pracownikiem jednostki wykonującej
   zadania zarządcy drogi lub audytor bezpieczeństwa ruchu drogowego
   niepozostający w stosunku pracy z jednostką wykonującą zadania zarządcy
   drogi.
5. W jednostce wykonującej zadania zarządcy drogi zatrudniającej audytora
   bezpieczeństwa ruchu drogowego tworzy się wieloosobowe lub jednoosobowe
   komórki audytu bezpieczeństwa ruchu drogowego.
6. Działalnością wieloosobowej komórki audytu bezpieczeństwa ruchu drogowego
   kieruje audytor bezpieczeństwa ruchu drogowego, zwany dalej "kierownikiem
   komórki audytu bezpieczeństwa ruchu drogowego".
7. Kierownik komórki audytu bezpieczeństwa ruchu drogowego podlega bezpośrednio
   kierownikowi jednostki, o której mowa w ust. 5, a w Generalnej Dyrekcji Dróg
   Krajowych i Autostrad - Generalnemu Dyrektorowi Dróg Krajowych i Autostrad.
8. Kierownik jednostki, o której mowa w ust. 5, a w Generalnej Dyrekcji Dróg
   Krajowych i Autostrad - Generalny Dyrektor Dróg Krajowych i Autostrad,
   zapewnia warunki niezbędne do niezależnego, obiektywnego i efektywnego
   prowadzenia audytu bezpieczeństwa ruchu drogowego, w tym zapewnia
   organizacyjną odrębność komórki audytu bezpieczeństwa ruchu drogowego.
9. Do audytora bezpieczeństwa ruchu drogowego zatrudnionego w jednoosobowej
   komórce audytu bezpieczeństwa ruchu drogowego stosuje się przepisy ustawy
   dotyczące kierownika komórki audytu bezpieczeństwa ruchu drogowego.

Art.24l
-------

1. Audyt bezpieczeństwa ruchu drogowego wymaga przedstawienia jego wyniku, na
   który składają się sprawozdanie oraz sformułowane na jego podstawie
   zalecenia dla zarządcy drogi.
2. Zarządca drogi uwzględnia wynik audytu bezpieczeństwa ruchu drogowego na
   dalszych etapach przygotowania, budowy i użytkowania drogi.
3. W uzasadnionych przypadkach zarządca drogi może nie uwzględnić wyniku audytu
   bezpieczeństwa ruchu drogowego.
4. W przypadku, o którym mowa w ust. 3, zarządca drogi jest obowiązany do
   opracowania uzasadnienia stanowiącego załącznik do wyniku audytu
   bezpieczeństwa ruchu drogowego.

Art.24m
-------

1. Klasyfikację odcinków dróg ze względu na koncentrację wypadków śmiertelnych
   oraz klasyfikację odcinków dróg ze względu na bezpieczeństwo sieci drogowej
   przeprowadza się co najmniej raz na trzy lata.
2. Klasyfikację odcinków dróg ze względu na koncentrację wypadków śmiertelnych
   przeprowadza się, w odniesieniu do natężenia ruchu, dla odcinków dróg
   pozostających w użytkowaniu nie mniej niż trzy lata.
3. Minister właściwy do spraw transportu określi, w drodze rozporządzenia,
   metodę dokonywania klasyfikacji, o których mowa w ust. 1, mając na uwadze
   liczbę wypadków drogowych, natężenie ruchu, rodzaj ruchu oraz poprawę
   poziomu bezpieczeństwa użytkowników dróg, a także zmniejszenie kosztów
   wypadków drogowych.
4. Wyniki klasyfikacji, o których mowa w ust. 1, podlegają ocenie wykonywanej
   przez zespół ekspertów powoływany przez zarządcę drogi. W skład zespołu
   ekspertów wchodzi co najmniej jedna osoba posiadająca kwalifikacje i
   doświadczenie wymienione w art. 24n ust. 2 pkt 4 i 5.
5. Zespół ekspertów, po przeprowadzeniu wizytacji w terenie, przedstawia
   zarządcy drogi propozycję działań, które należy podjąć w celu poprawy
   bezpieczeństwa ruchu drogowego, mając na uwadze propozycje działań
   przedstawione po przeprowadzeniu ostatniej klasyfikacji, o której mowa w
   ust. 1, oraz sprawozdań, o których mowa w art. 130b ust. 1 ustawy z dnia 20
   czerwca 1997 r. - Prawo o ruchu drogowym.
6. Zarządca drogi przeprowadza analizę kosztów i korzyści proponowanych
   działań, o których mowa w ust. 5. Działania wybrane przez zarządcę drogi są
   realizowane w ramach dostępnych środków finansowych.
7. Zarządca drogi jest obowiązany do poinformowania uczestników ruchu drogowego
   o wynikach klasyfikacji, o której mowa w ust. 2, na swojej stronie
   podmiotowej Biuletynu Informacji Publicznej.

Rozdział 2c — Audytorzy bezpieczeństwa ruchu drogowego
******************************************************


Art.24n
-------

1. Audytorem bezpieczeństwa ruchu drogowego jest osoba, która posiada
   certyfikat audytora bezpieczeństwa ruchu drogowego.
2. Certyfikat audytora bezpieczeństwa ruchu drogowego może uzyskać osoba,
   która:

   1) posiada pełną zdolność do czynności prawnych;
   2) korzysta z pełni z praw publicznych;
   3) nie została skazana prawomocnym wyrokiem sądu za umyślne przestępstwo lub
      umyślne przestępstwo skarbowe;
   4) posiada wyższe wykształcenie techniczne w zakresie budownictwa drogowego,
      inżynierii ruchu drogowego lub transportu;
   5) posiada co najmniej 5-letnią praktykę w zakresie projektowania dróg,
      inżynierii ruchu drogowego, zarządzania drogami, zarządzania ruchem
      drogowym lub opiniowania projektów drogowych pod względem bezpieczeństwa
      ruchu drogowego;
   6) posiada zaświadczenie o ukończeniu szkolenia i zdaniu egzaminu na
      audytora bezpieczeństwa ruchu drogowego.

3. Audytor bezpieczeństwa ruchu drogowego ma obowiązek stałego podnoszenia
   kwalifikacji zawodowych na szkoleniach okresowych co najmniej raz na trzy
   lata.
4. Szkolenia zakończone egzaminem, o których mowa w ust. 2 pkt 6, oraz
   szkolenia okresowe, o których mowa w ust. 3, przeprowadzają uczelnie, o
   których mowa w art. 2 ust. 1 pkt 2 i 3 ustawy z dnia 27 lipca 2005 r. -
   Prawo o szkolnictwie wyższym (Dz. U. z 2012 r. poz. 572, z późn. zm.) oraz
   instytuty badawcze posiadające kategorię A, działające w zakresie
   budownictwa drogowego na podstawie ustawy z dnia 30 kwietnia 2010 r. o
   instytutach badawczych (Dz. U. Nr 96, poz. 618, z późn. zm.).
5. Uczelnie, o których mowa w art. 2 ust. 1 pkt 2 i 3 ustawy z dnia 27 lipca
   2005 r. - Prawo o szkolnictwie wyższym, powinny posiadać uprawnienia do
   prowadzenia studiów drugiego stopnia z zakresu budownictwa drogowego.
6. Certyfikat audytora bezpieczeństwa ruchu drogowego wydaje, na wniosek
   kandydata na audytora bezpieczeństwa ruchu drogowego, na okres trzech lat
   minister właściwy do spraw transportu, po dostarczeniu przez tego kandydata
   oświadczenia o spełnieniu wymogów, o których mowa w ust. 2 pkt 1 i 2, oraz
   dokumentów potwierdzających spełnienie wymogów, o których mowa w ust. 2 pkt
   3-6.
7. Certyfikat audytora bezpieczeństwa ruchu drogowego jest przedłużany na
   kolejne trzy lata na wniosek audytora bezpieczeństwa ruchu drogowego po
   dostarczeniu potwierdzenia ukończenia szkolenia okresowego, o którym mowa w
   ust. 3, oświadczenia o spełnieniu wymogów, o których mowa w ust. 2 pkt 1 i
   2, oraz dokumentu potwierdzającego spełnienie wymogu, o którym mowa w ust. 2
   pkt 3.
8. Minister właściwy do spraw transportu odmawia w drodze decyzji
   administracyjnej:

   1) wydania certyfikatu audytora bezpieczeństwa ruchu drogowego w przypadku
      niespełnienia któregokolwiek z wymogów, o których mowa w ust. 2;
   2) przedłużenia certyfikatu audytora bezpieczeństwa ruchu drogowego w
      przypadku niespełnienia któregokolwiek z wymogów, o których mowa w
      ust. 7.

9. Minister właściwy do spraw transportu określi, w drodze rozporządzenia:

   1) sposób przeprowadzania i zakres programowy szkolenia zakończonego
      egzaminem, o którym mowa w ust. 2 pkt 6,
   2) sposób przeprowadzania i zakres programowy szkolenia okresowego, o którym
      mowa w ust. 3,
   3) wzór certyfikatu audytora bezpieczeństwa ruchu drogowego

- mając na uwadze zapewnienie odpowiedniego poziomu edukacyjnego szkoleń oraz
  jednolitego wzoru dokumentu.

Art.24o
-------

Audytor bezpieczeństwa ruchu drogowego traci certyfikat audytora bezpieczeństwa
ruchu drogowego w przypadku:

1) skazania prawomocnym wyrokiem za umyślne przestępstwo lub umyślne
   przestępstwo skarbowe;
2) pozbawienia praw publicznych;
3) całkowitego lub częściowego ubezwłasnowolnienia.

Rozdział 3 — Skrzyżowania dróg z innymi drogami komunikacji lądowej i powietrznej oraz liniowymi urządzeniami technicznymi
**************************************************************************************************************************


Art.25
------

1. Budowa, przebudowa, remont, utrzymanie i ochrona skrzyżowań dróg różnej
   kategorii, wraz z drogowymi obiektami inżynierskimi w pasie drogowym oraz
   urządzeniami bezpieczeństwa i organizacji ruchu, związanymi z
   funkcjonowaniem tego skrzyżowania, należy do zarządcy drogi właściwego dla
   drogi wyższej kategorii.
2. Koszt budowy lub przebudowy skrzyżowania, o którym mowa w ust. 1, wraz z
   koniecznymi drogowymi obiektami inżynierskimi w pasie drogowym oraz
   urządzeniami bezpieczeństwa i organizacji ruchu, związanymi z
   funkcjonowaniem tego skrzyżowania, ponosi zarządca drogi, który wystąpił z
   inicjatywą budowy lub przebudowy takiego skrzyżowania.
3. Budowa, przebudowa, remont, utrzymanie i ochrona skrzyżowania autostrady lub
   drogi ekspresowej z innymi drogami publicznymi, wraz z drogowymi obiektami
   inżynierskimi w pasie drogowym oraz urządzeniami bezpieczeństwa i
   organizacji ruchu, związanymi z funkcjonowaniem tego skrzyżowania, należy do
   zarządcy autostrady lub drogi ekspresowej.
4. Koszt budowy lub przebudowy skrzyżowania, o którym mowa w ust. 3, wraz z
   koniecznymi drogowymi obiektami inżynierskimi w pasie drogowym oraz
   urządzeniami bezpieczeństwa i organizacji ruchu, związanymi z
   funkcjonowaniem tego skrzyżowania, ponosi zarządca drogi, który wystąpił z
   inicjatywą budowy lub przebudowy takiego skrzyżowania.

Art.26
------

Budowa, przebudowa, remont, utrzymanie i ochrona budowli brzegowych przepraw
promowych zlokalizowanych w ciągu drogi wraz z urządzeniami i instalacjami oraz
jednostkami przeprawowymi (promami) należy do zarządcy tej drogi.

Art.27
------

(uchylony)

Art.28
------

1. Budowa, przebudowa, remont, utrzymanie i ochrona skrzyżowań dróg z liniami
   kolejowymi w poziomie szyn, wraz z zaporami, urządzeniami sygnalizacyjnymi,
   znakami kolejowymi, jak również nawierzchnią drogową w obszarze między
   rogatkami, a w przypadku ich braku - w odległości 4 m od skrajnych szyn,
   należy do zarządu kolei.
2. Do zarządów kolei należy również:

   1) konserwacja znajdującej się nad skrajnią kolejową dolnej części
      konstrukcji wiaduktów drogowych, łącznie z urządzeniami
      zabezpieczającymi;
   2) budowa i utrzymanie urządzeń odwadniających wiadukty kolejowe nad
      drogami, łącznie z urządzeniami odprowadzającymi wodę poza koronę drogi;
   3) budowa skrzyżowań dróg z torami kolejowymi w różnych poziomach w razie
      budowy nowej lub zmiany trasy istniejącej linii kolejowej, zwiększenia
      ilości torów, elektryfikacji linii, zwiększenia szybkości lub
      częstotliwości ruchu pociągów.

Art.28a
-------

1. Budowa, przebudowa, remont, utrzymanie i ochrona torowiska tramwajowego
   umieszczonego w pasie drogowym należy do podmiotu zarządzającego torowiskiem
   tramwajowym.
2. Podmiot zarządzający torowiskiem tramwajowym, o którym mowa w ust. 1,
   uzgadnia z zarządcą drogi warunki wykonania robót na terenie tego torowiska.

Art.29
------

1. Budowa lub przebudowa zjazdu należy do właściciela lub użytkownika
   nieruchomości przyległych do drogi, po uzyskaniu, w drodze decyzji
   administracyjnej, zezwolenia zarządcy drogi na lokalizację zjazdu lub
   przebudowę zjazdu, z zastrzeżeniem ust. 2.
2. W przypadku budowy lub przebudowy drogi budowa lub przebudowa zjazdów
   dotychczas istniejących należy do zarządcy drogi.
3. Zezwolenie na lokalizację zjazdu, o którym mowa w ust. 1, wydaje się na czas
   nieokreślony, z zastrzeżeniem ust. 5. W zezwoleniu na lokalizację zjazdu
   określa się miejsce lokalizacji zjazdu i jego parametry techniczne, a w
   zezwoleniu na przebudowę zjazdu - jego parametry techniczne, a także
   zamieszcza się, w przypadku obu zezwoleń, pouczenie o obowiązku:

   1) uzyskania przed rozpoczęciem prac budowlanych pozwolenia na budowę, a w
      przypadku przebudowy zjazdu dokonania zgłoszenia budowy albo wykonania
      robót budowlanych oraz uzyskania zezwolenia zarządcy drogi na prowadzenie
      robót w pasie drogowym;
   2) uzgodnienia z zarządcą drogi, przed uzyskaniem pozwolenia na budowę,
      projektu budowlanego zjazdu.

4. Ze względu na wymogi wynikające z warunków technicznych, jakim powinny
   odpowiadać drogi publiczne, zarządca drogi może odmówić wydania zezwolenia
   na lokalizację zjazdu lub jego przebudowę albo wydać zezwolenie na
   lokalizację zjazdu na czas określony.
5. Decyzja o wydaniu zezwolenia na lokalizację zjazdu, o którym mowa w ust. 1,
   wygasa, jeżeli w ciągu 3 lat od jego wydania zjazd nie został wybudowany.

Art.29a
-------

1. Za wybudowanie lub przebudowę zjazdu:

   1) bez zezwolenia zarządcy drogi,
   2) o powierzchni większej niż określona w zatwierdzonym projekcie budowlanym
      zjazdu

   – zarządca drogi wymierza, w drodze decyzji administracyjnej, karę pieniężną
   w wysokości 10-krotności opłaty ustalanej zgodnie z art. 40 ust. 4.
2. Karę, o której mowa w ust. 1, zarządca drogi wymierza również za użytkowanie
   zjazdu po terminie określonym w zezwoleniu zarządcy drogi.
3. Termin płatności kary, o której mowa w ust. 1, wynosi 14 dni od dnia, w
   którym decyzja ustalająca jej wysokość stała się ostateczna.

Art.30
------

Utrzymywanie zjazdów, łącznie ze znajdującymi się pod nimi przepustami, należy
do właścicieli lub użytkowników gruntów przyległych do drogi.

Art.31
------

(uchylony)

Art.32
------

1. W przypadku gdy budowa lub przebudowa drogi w miejscu jej przecięcia się z
   inną drogą transportu lądowego - z wyjątkiem skrzyżowania z linią kolejową w
   poziomie szyn, o którym mowa w art. 28 ust. 1 - wodnego, korytarzem
   powietrznym w strefie lotniska lub urządzeniem typu liniowego (w
   szczególności linią energetyczną lub telekomunikacyjną, rurociągiem,
   taśmociągiem) powoduje naruszenie tych obiektów lub urządzeń albo
   konieczność zmian dotychczasowego ich stanu, przywrócenie poprzedniego stanu
   lub dokonanie zmiany należy do zarządcy drogi, z zastrzeżeniem ust. 2-4.
2. Koszty przyłączy do urządzeń liniowych w granicach pasa drogowego, z
   zastrzeżeniem ust. 4, pokrywa w całości zarządca drogi, a poza tymi
   granicami właściciel lub użytkownik urządzeń.
3. Koszty przełożenia urządzeń liniowych w pasie drogowym, wynikające z
   naruszenia lub konieczności zmian stanu dotychczasowego urządzenia
   liniowego, w wysokości odpowiadającej wartości tych urządzeń i przy
   zachowaniu dotychczasowych właściwości użytkowych i parametrów technicznych,
   z zastrzeżeniem ust. 4, pokrywa zarządca drogi.
4. Jeżeli w wyniku uzgodnień zarządcy drogi z zainteresowaną stroną zostaną
   wprowadzone ulepszenia urządzeń, koszty tych ulepszeń pokrywa odpowiednio
   ich właściciel lub użytkownik.
5. Przekazanie przez zarządcę drogi urządzeń, o których mowa w ust. 2-4,
   wykonanych w pasie drogowym, osobie uprawnionej następuje nieodpłatnie, na
   podstawie protokołu zdawczo-odbiorczego.

Art.33
------

Wykonanie skrzyżowań nowo budowanych lub przebudowywanych obiektów, o których
mowa w art. 32 ust. 1, oraz linii kolejowych, powodujące naruszenie stanu
istniejącej drogi lub konieczność dokonania zmian elementów drogi, należy do
inwestora zlecającego budowę lub przebudowę tych obiektów.

Rozdział 4 — Pas drogowy
************************


Art.34
------

Odległość granicy pasa drogowego od zewnętrznej krawędzi wykopu, nasypu, rowu
lub od innych urządzeń wymienionych w art. 4 pkt 1 i 2 powinna wynosić co
najmniej 0,75 m, a dla autostrad i dróg ekspresowych - co najmniej 2 m.

Art.35
------

1. Zarządca drogi sporządza i weryfikuje okresowo plany rozwoju sieci drogowej
   i przekazuje je, niezwłocznie po sporządzeniu, organom właściwym w sprawie
   sporządzania planu zagospodarowania przestrzennego.
2. W planach zagospodarowania przestrzennego województwa i miejscowych planach
   zagospodarowania przestrzennego przeznacza się pod przyszłą budowę dróg pas
   terenu o szerokości uwzględniającej ochronę użytkowników dróg i terenu
   przyległego przed wzajemnym niekorzystnym oddziaływaniem.
3. Zmianę zagospodarowania terenu przyległego do pasa drogowego, w
   szczególności polegającą na budowie obiektu budowlanego lub wykonaniu innych
   robót budowlanych, a także zmianę sposobu użytkowania obiektu budowlanego
   lub jego części, zarządca drogi uzgadnia w zakresie możliwości włączenia do
   drogi ruchu drogowego spowodowanego tą zmianą.
4. W pasie terenu, o którym mowa w ust. 2, mogą być wznoszone tylko tymczasowe
   obiekty budowlane oraz urządzenia budowlane związane z obiektami
   budowlanymi. Ich usunięcie w wypadku budowy drogi następuje na koszt
   właściciela, bez odszkodowania.
5. Nieruchomości położone w pasie, o którym mowa w ust. 2, stanowiące własność
   Skarbu Państwa, przeznaczone pod przyszłą budowę drogi, nie mogą być zbyte
   bez zgody właściwego zarządcy drogi.

Art.36
------

W przypadku zajęcia pasa drogowego bez zezwolenia zarządcy drogi lub niezgodnie
z warunkami podanymi w tym zezwoleniu właściwy zarządca drogi orzeka, w drodze
decyzji administracyjnej, o jego przywróceniu do stanu poprzedniego. Przepisu
tego nie stosuje się w przypadku zajęcia pasa drogowego bez zezwolenia zarządcy
drogi lub niezgodnie z warunkami podanymi w tym zezwoleniu, wymagającego
podjęcia przez właściwy organ nadzoru budowlanego decyzji o rozbiórce obiektu
budowlanego.

Art.37
------

(uchylony).

Art.38
------

1. Istniejące w pasie drogowym obiekty budowlane i urządzenia niezwiązane z
   gospodarką drogową lub obsługą ruchu, które nie powodują zagrożenia i
   utrudnień ruchu drogowego i nie zakłócają wykonywania zadań zarządu drogi,
   mogą pozostać w dotychczasowym stanie.
2. Przebudowa lub remont obiektów budowlanych lub urządzeń, o których mowa w
   ust. 1, wymaga zgody zarządcy drogi, a w przypadku gdy planowane roboty są
   objęte obowiązkiem uzyskania pozwolenia na budowę, również uzgodnienia
   projektu budowlanego.
3. Wyrażenie zgody, o której mowa w ust. 2, powinno nastąpić w terminie 14 dni
   od dnia wystąpienia z wnioskiem o taką zgodę. Niezajęcie stanowiska w tym
   terminie uznaje się jako wyrażenie zgody. Odmowa wyrażenia zgody następuje w
   drodze decyzji administracyjnej.

Art.39
------

1. Zabrania się dokonywania w pasie drogowym czynności, które mogłyby powodować
   niszczenie lub uszkodzenie drogi i jej urządzeń albo zmniejszenie jej
   trwałości oraz zagrażać bezpieczeństwu ruchu drogowego. W szczególności
   zabrania się:

   1) lokalizacji obiektów budowlanych, umieszczania urządzeń, przedmiotów i
      materiałów niezwiązanych z potrzebami zarządzania drogami lub potrzebami
      ruchu drogowego;
   2) włóczenia po drogach oraz porzucania na nich przedmiotów lub używania
      pojazdów niszczących nawierzchnię drogi;
   3) poruszania się po drogach pojazdów nienormatywnych bez wymaganego
      zezwolenia lub w sposób niezgodny z przepisami ruchu drogowego;
   4) samowolnego ustawiania, zmieniania i uszkadzania znaków drogowych i
      urządzeń ostrzegawczo-zabezpieczających;
   5) umieszczania reklam poza obszarami zabudowanymi, z wyjątkiem parkingów;
   6) umieszczania urządzeń zastępujących obowiązujące znaki drogowe;
   7) niszczenia rowów, skarp, nasypów i wykopów oraz samowolnego rozkopywania
      drogi;
   8) zaorywania lub zwężania w inny sposób pasa drogowego;
   9) odprowadzania wody i ścieków z urządzeń melioracyjnych, gospodarskich lub
      zakładowych do rowów przydrożnych lub na jezdnię drogi;
   10) wypasania zwierząt gospodarskich;
   11) rozniecania ognisk w pobliżu drogowych obiektów inżynierskich i przepraw
       promowych oraz przejeżdżania przez nie z otwartym ogniem;
   12) usuwania, niszczenia i uszkadzania zadrzewień przydrożnych.

1. a) Przepisu ust. 1 pkt 1 nie stosuje się do umieszczania, konserwacji,
   przebudowy i naprawy infrastruktury telekomunikacyjnej w rozumieniu ustawy z
   dnia 16 lipca 2004 r. - Prawo telekomunikacyjne (Dz. U. z 2014 r. poz. 243,
   827 i 1198) oraz urządzeń służących do doprowadzania lub odprowadzania
   płynów, pary, gazu, energii elektrycznej oraz urządzeń związanych z ich
   eksploatacją, a także do innych czynności związanych z eksploatacją tej
   infrastruktury i urządzeń, jeżeli warunki techniczne i wymogi bezpieczeństwa
   na to pozwalają.
2. (uchylony).
3. W szczególnie uzasadnionych przypadkach lokalizowanie w pasie drogowym
   obiektów budowlanych lub urządzeń niezwiązanych z potrzebami zarządzania
   drogami lub potrzebami ruchu drogowego może nastąpić wyłącznie za
   zezwoleniem właściwego zarządcy drogi, z zastrzeżeniem ust. 7, wydawanym w
   drodze decyzji administracyjnej. Jednakże właściwy zarządca drogi może
   odmówić wydania zezwolenia na umieszczenie w pasie drogowym urządzeń i
   infrastruktury, o których mowa w ust. 1a, wyłącznie, jeżeli ich umieszczenie
   spowodowałoby zagrożenie bezpieczeństwa ruchu drogowego, naruszenie wymagań
   wynikających z przepisów odrębnych lub miałoby doprowadzić do utraty
   uprawnień z tytułu gwarancji lub rękojmi w zakresie budowy, przebudowy lub
   remontu drogi.

3. a) W decyzji, o której mowa w ust. 3, określa się w szczególności: rodzaj
   inwestycji, sposób, miejsce i warunki jej umieszczenia w pasie drogowym oraz
   pouczenie inwestora, że przed rozpoczęciem robót budowlanych jest
   zobowiązany do:

   1) uzyskania pozwolenia na budowę lub zgłoszenia budowy albo wykonywania
      robót budowlanych;
   2) uzgodnienia z zarządcą drogi, przed uzyskaniem pozwolenia na budowę,
      projektu budowlanego obiektu lub urządzenia, o którym mowa w ust. 3;
   3) uzyskania zezwolenia zarządcy drogi na zajęcie pasa drogowego,
      dotyczącego prowadzenia robót w pasie drogowym lub na umieszczenie w nim
      obiektu lub urządzenia.

3. b) W przypadku gdy właściwy organ nie wyda decyzji, o której mowa w ust. 3,
   w terminie 65 dni od dnia złożenia wniosku, organ wyższego stopnia, a w
   przypadku braku takiego organu organ nadzorujący, wymierza temu organowi, w
   drodze postanowienia, na które przysługuje zażalenie, karę pieniężną w
   wysokości 500 zł za każdy dzień zwłoki. Wpływy z kar pieniężnych stanowią
   dochód budżetu państwa.

3. c) Karę pieniężną uiszcza się w terminie 14 dni od dnia doręczenia
   postanowienia, o którym mowa w ust. 3b. W przypadku nieuiszczenia kary
   pieniężnej, o której mowa w ust. 3b, podlega ona ściągnięciu w trybie
   przepisów o postępowaniu egzekucyjnym w administracji.

3. d) Do terminu, o którym mowa w ust. 3b, nie wlicza się terminów
   przewidzianych w przepisach prawa do dokonania określonych czynności,
   okresów zawieszenia postępowania oraz okresów opóźnień spowodowanych z winy
   strony albo z przyczyn niezależnych od organu.

4. Utrzymanie obiektów i urządzeń, o których mowa w ust. 3, należy do ich
   posiadaczy.
5. Jeżeli budowa, przebudowa lub remont drogi wymaga przełożenia urządzenia lub
   obiektu, o którym mowa w ust. 3, koszt tego przełożenia ponosi jego
   właściciel.
6. Zarządca drogi w trakcie budowy lub przebudowy drogi jest obowiązany
   zlokalizować kanał technologiczny w pasie drogowym:

   1) dróg krajowych;
   2) pozostałych dróg publicznych, chyba że w terminie 60 dni od dnia
      ogłoszenia informacji, o której mowa w ust. 6a, nie zgłoszono
      zainteresowania udostępnieniem kanału technologicznego.

6. a) Najpóźniej na 6 miesięcy przed dniem złożenia wniosku o wydanie decyzji o
   środowiskowych uwarunkowaniach, o zezwoleniu na realizację inwestycji
   drogowej, o pozwoleniu na budowę albo dniem zgłoszenia przebudowy dróg, o
   których mowa w ust. 6 pkt 2, zarządca drogi zamieszcza na swojej stronie
   internetowej informację o zamiarze rozpoczęcia budowy lub przebudowy drogi i
   możliwości zgłaszania zainteresowania z udostępnieniem kanału
   technologicznego, jednocześnie zawiadamiając o tym Prezesa Urzędu
   Komunikacji Elektronicznej, zwanego dalej "Prezesem UKE".

6. b) Podmiot, który zgłosi zainteresowanie udostępnieniem przez zarządcę drogi
   kanału technologicznego, a następnie po jego wybudowaniu nie złoży oferty,
   jest obowiązany zwrócić zarządcy drogi koszty wybudowania kanału
   technologicznego, o ile nie udostępniono tego kanału innym podmiotom.

6. c) Minister właściwy do spraw łączności, na wniosek zarządcy drogi w drodze
   decyzji, zwalnia zarządcę z obowiązku budowy kanału technologicznego, jeżeli
   w pobliżu pasa drogowego istnieje już kanał technologiczny lub linia
   światłowodowa, posiadające wolne zasoby wystarczające do zaspokojenia
   potrzeb społecznych w zakresie dostępu do usług szerokopasmowych lub w
   sytuacji, gdy lokalizowanie kanału technologicznego byłoby ekonomicznie
   nieracjonalne lub technicznie niemożliwe. W odniesieniu do dróg, o których
   mowa w ust. 6 pkt 2, zwolnienie następuje przed ogłoszeniem, o którym mowa w
   ust. 6a.

6. d) Niezwłocznie po wybudowaniu kanału technologicznego, a jeszcze przed jego
   udostępnieniem innym podmiotom, zarządca drogi przekazuje Prezesowi UKE
   informację o przebiegu nowo zlokalizowanego kanału technologicznego.

6. e) Zarządca drogi, na wniosek Prezesa UKE lub przedsiębiorcy
   telekomunikacyjnego, udziela informacji o kanałach technologicznych
   zlokalizowanych w pasie drogowym na obszarze jego właściwości.
7. Zarządca drogi udostępnia kanały technologiczne za opłatą, w drodze umowy
   dzierżawy lub najmu, na zasadach określonych w ust. 7a-7f.

7. a) Zarządca drogi zamieszcza w Biuletynie Informacji Publicznej na właściwej
   dla niego stronie podmiotowej informację o zamiarze udostępnienia kanału
   technologicznego, podając lokalizację kanału, zakres wolnych zasobów w
   kanale, jego podstawowe parametry techniczne, termin i miejsce składania
   ofert, wymagania formalne dotyczące ofert oraz kryteria wyboru w przypadku
   złożenia ofert przekraczających zakres wolnych zasobów, z uwzględnieniem
   ust. 7e.

7. b) Informację, o której mowa w ust. 7a, zarządca drogi zamieszcza także w
   przypadku, gdy przed ogłoszeniem zamiaru udostępnienia kanału
   technologicznego wpłynie do niego wniosek o udostępnienie tego kanału. W
   przypadku gdy do zarządcy drogi wpłynie pierwszy wniosek o udostępnienie
   kanału technologicznego, ogłoszenie jest publikowane nie później niż w
   terminie 14 dni od dnia wpłynięcia tego wniosku.

7. c) Zarządca drogi jednocześnie z zamieszczeniem informacji, o której mowa w
   ust. 7a i 7b, zawiadamia o tym fakcie Prezesa UKE. Prezes UKE niezwłocznie
   zamieszcza w Biuletynie Informacji Publicznej na swojej stronie podmiotowej
   informację o ogłoszeniu, wraz z odesłaniem do strony Biuletynu Informacji
   Publicznej, na której informacja została opublikowana. Termin składania
   ofert nie może być krótszy niż 14 dni od dnia zamieszczenia informacji przez
   zarządcę drogi.

7. d) Zarządca drogi jest obowiązany zawrzeć umowę dzierżawy lub najmu kanału
   technologicznego najpóźniej w terminie 21 dni od dnia, w którym upłynął
   termin składania ofert.

7. e) W przypadku gdy z powodu braku wolnych zasobów w kanale technologicznym
   jest niemożliwe uwzględnienie wszystkich ofert na udostępnienie tego kanału,
   zarządca drogi dokonuje wyboru podmiotu, któremu udostępnia kanał
   technologiczny, stosując kryteria wyboru określone w informacji, o której
   mowa w ust. 7a, przestrzegając zasad przejrzystości, równego traktowania
   zainteresowanych podmiotów, a także pierwszeństwa dla umieszczania w kanale
   technologicznym linii światłowodowych przeznaczonych na potrzeby dostępu do
   telekomunikacyjnych usług szerokopasmowych.

7. f) Za udostępnienie kanału technologicznego pobiera się opłaty w wysokości
   określonej w umowie, przy czym opłaty te są ustalane na poziomie kosztów
   budowy i utrzymania kanału. Przepisu art. 40 ust. 3 nie stosuje się do linii
   telekomunikacyjnych i elektroenergetycznych oraz innych urządzeń
   umieszczanych w kanale technologicznym.

8. Wykonywanie zadań związanych z zarządzaniem i utrzymywaniem kanałów
   technologicznych, o których mowa w ust. 6, zarządca drogi może powierzyć, w
   drodze umowy, podmiotowi wyłonionemu w drodze przetargu, z zachowaniem
   przepisów o zamówieniach publicznych lub w trybie określonym w ustawie z
   dnia 9 stycznia 2009 r. o koncesji na roboty budowlane lub usługi.

Art.40
------

1. Zajęcie pasa drogowego na cele niezwiązane z budową, przebudową, remontem,
   utrzymaniem i ochroną dróg wymaga zezwolenia zarządcy drogi, w drodze
   decyzji administracyjnej.
2. Zezwolenie, o którym mowa w ust. 1, dotyczy:

   1) prowadzenia robót w pasie drogowym;
   2) umieszczania w pasie drogowym urządzeń infrastruktury technicznej
      niezwiązanych z potrzebami zarządzania drogami lub potrzebami ruchu
      drogowego;
   3) umieszczania w pasie drogowym obiektów budowlanych niezwiązanych z
      potrzebami zarządzania drogami lub potrzebami ruchu drogowego oraz
      reklam;
   4) zajęcia pasa drogowego na prawach wyłączności w celach innych niż
      wymienione w pkt 1-3.

3. Za zajęcie pasa drogowego pobiera się opłatę.
4. Opłatę za zajęcie pasa drogowego w celu, o którym mowa w ust. 2 pkt 1 i 4,
   ustala się jako iloczyn liczby metrów kwadratowych zajętej powierzchni pasa
   drogowego, stawki opłaty za zajęcie 1 m2pasa drogowego i liczby dni
   zajmowania pasa drogowego, przy czym zajęcie pasa drogowego przez okres
   krótszy niż 24 godziny jest traktowane jak zajęcie pasa drogowego przez 1
   dzień.
5. Opłatę za zajęcie pasa drogowego w celu, o którym mowa w ust. 2 pkt 2,
   ustala się jako iloczyn liczby metrów kwadratowych powierzchni pasa
   drogowego zajętej przez rzut poziomy urządzenia i stawki opłaty za zajęcie 1
   m2pasa drogowego pobieranej za każdy rok umieszczenia urządzenia w pasie
   drogowym, przy czym za umieszczenie urządzenia w pasie drogowym lub na
   drogowym obiekcie inżynierskim przez okres krótszy niż rok opłata obliczana
   jest proporcjonalnie do liczby dni umieszczenia urządzenia w pasie drogowym
   lub na drogowym obiekcie inżynierskim.
6. Opłatę za zajęcie pasa drogowego w celu, o którym mowa w ust. 2 pkt 3,
   ustala się jako iloczyn liczby metrów kwadratowych powierzchni pasa
   drogowego zajętej przez rzut poziomy obiektu budowlanego albo powierzchni
   reklamy, liczby dni zajmowania pasa drogowego i stawki opłaty za zajęcie 1
   m2pasa drogowego.
7. Minister właściwy do spraw transportu, w drodze rozporządzenia, ustala, z
   uwzględnieniem przepisów o pomocy publicznej, dla dróg, których zarządcą
   jest Generalny Dyrektor Dróg Krajowych i Autostrad, wysokość stawek opłaty
   za zajęcie 1 m2pasa drogowego. Stawki opłaty, o których mowa w ust. 4 i 6,
   nie mogą przekroczyć 10 zł za jeden dzień zajmowania pasa drogowego, a
   stawka opłaty, o której mowa w ust. 5, nie może przekroczyć 200 zł, z tym że
   w odniesieniu do obiektów i urządzeń infrastruktury telekomunikacyjnej
   stawki opłaty, o których mowa w ust. 4 i 6, nie mogą przekroczyć 0,20 zł za
   jeden dzień zajmowania pasa drogowego, a stawka opłaty, o której mowa w
   ust. 5, nie może przekroczyć 20 zł.
8. Organ stanowiący jednostki samorządu terytorialnego, w drodze uchwały,
   ustala dla dróg, których zarządcą jest jednostka samorządu terytorialnego,
   wysokość stawek opłaty za zajęcie 1 m2pasa drogowego, z tym że stawki
   opłaty, o których mowa w ust. 4 i 6, nie mogą przekroczyć 10 zł za jeden
   dzień zajmowania pasa drogowego, a stawka opłaty, o której mowa w ust. 5,
   nie może przekroczyć 200 zł.
9. Przy ustalaniu stawek, o których mowa w ust. 7 i 8, uwzględnia się:

   1) kategorię drogi, której pas drogowy zostaje zajęty;
   2) rodzaj elementu zajętego pasa drogowego;
   3) procentową wielkość zajmowanej szerokości jezdni;
   4) rodzaj zajęcia pasa drogowego;
   5) rodzaj urządzenia lub obiektu budowlanego umieszczonego w pasie drogowym.

10. Zajęcie pasa drogowego o powierzchni mniejszej niż 1 m2lub powierzchni pasa
    drogowego zajętej przez rzut poziomy obiektu budowlanego lub urządzenia
    mniejszej niż 1 m2jest traktowane jak zajęcie 1 m2pasa drogowego.
11. Opłatę, o której mowa w ust. 3, ustala, w drodze decyzji administracyjnej,
    właściwy zarządca drogi przy udzielaniu zezwolenia na zajęcie pasa
    drogowego.
12. Za zajęcie pasa drogowego:

    1) bez zezwolenia zarządcy drogi,
    2) z przekroczeniem terminu zajęcia określonego w zezwoleniu zarządcy
       drogi,
    3) o powierzchni większej niż określona w zezwoleniu zarządcy drogi

    – zarządca drogi wymierza, w drodze decyzji administracyjnej, karę
    pieniężną w wysokości 10-krotności opłaty ustalanej zgodnie z ust. 4-6.
13. Termin uiszczenia opłaty, o której mowa w ust. 3, oraz kary, o której mowa
    w ust. 12, wynosi 14 dni od dnia, w którym decyzja ustalająca ich wysokość
    stała się ostateczna, z zastrzeżeniem ust. 13a.

13. a) Opłatę roczną, o której mowa w ust. 5, za pierwszy rok umieszczenia
    urządzenia w pasie drogowym uiszcza się w terminie określonym w ust. 13, a
    za lata następne w terminie do dnia 15 stycznia każdego roku, z góry za
    dany rok.
14. Przepisu ust. 1 nie stosuje się w razie konieczności usunięcia awarii
    urządzeń niezwiązanych z potrzebami zarządzania drogami lub potrzebami
    ruchu drogowego, a znajdujących się w pasie drogowym. Po zlokalizowaniu
    awarii prowadzący roboty niezwłocznie zawiadamia o tym zarządcę drogi i w
    porozumieniu z nim określa termin i powierzchnię zajętego pasa drogowego.

14. a) Zarządca drogi określa, w drodze decyzji administracyjnej, warunki
    zajęcia pasa drogowego, o którym mowa w ust. 14, oraz warunki jego
    przywrócenia do stanu poprzedniego, a także ustala wysokość opłaty, o
    której mowa w ust. 4.

14. b) Za wejście w pas drogowy, o którym mowa w ust. 14, bez zawiadomienia
    zarządcy drogi, przekroczenie ustalonego terminu i powierzchni zajęcia pasa
    drogowego zarządca drogi wymierza karę pieniężną zgodnie z ust. 12.
15. Zajmujący pas drogowy jest obowiązany zapewnić bezpieczne warunki ruchu i
    przywrócić pas do poprzedniego stanu użyteczności w określonym terminie.
16. Rada Ministrów, w drodze rozporządzenia, określa warunki niezbędne do
    udzielania zezwoleń na zajmowanie pasa drogowego na cele, o których mowa w
    ust. 2, mając na względzie bezpieczeństwo użytkowania i ochronę dróg.

Art.40a
-------

1. Opłaty określone w art. 13 ust. 1 i 2, z wyłączeniem opłaty elektronicznej,
   w art. 13f ust. 1 i art. 40 ust. 3 oraz kary pieniężne określone w art. 13k
   ust. 1 i 2, art. 29a ust. 1 i 2 oraz w art. 40 ust. 12, a także opłaty z
   tytułu umów zawieranych na podstawie art. 22 ust. 2 i art. 39 ust. 7 są
   przekazywane odpowiednio do budżetów jednostek samorządu terytorialnego lub
   na wyodrębniony rachunek bankowy Generalnej Dyrekcji Dróg Krajowych i
   Autostrad.

1. a) W przypadku zawarcia umowy o partnerstwie publiczno-prywatnym opłaty
   pobierane przez partnera prywatnego, o których mowa w art. 13 ust. 1 pkt 1,
   art. 13 ust. 2 pkt 1, oraz opłata dodatkowa, o której mowa w art. 13f
   ust. 1, stanowią przychody partnera prywatnego.

1. b) W przypadku powierzenia drogowej spółce specjalnego przeznaczenia
   pobierania opłat, o których mowa w art. 13 ust. 2 pkt 1, mogą one stanowić
   przychód tej spółki, jeżeli umowa, o której mowa w art. 6 ust. 1 ustawy z
   dnia 12 stycznia 2007 r. o drogowych spółkach specjalnego przeznaczenia, tak
   stanowi.
2. (uchylony).
3. Środki z opłat i kar gromadzone na wyodrębnionym rachunku bankowym
   Generalnej Dyrekcji Dróg Krajowych i Autostrad, o którym mowa w ust. 1, są
   przekazywane w terminie pierwszych dwóch dni roboczych po zakończeniu
   tygodnia, w którym wpłynęły, na rachunek Krajowego Funduszu Drogowego, z
   przeznaczeniem na budowę lub przebudowę dróg krajowych, drogowych obiektów
   inżynierskich i przepraw promowych oraz na zakup urządzeń do ważenia
   pojazdów.
4. Środki z opłat elektronicznych są przekazywane na rachunek Krajowego
   Funduszu Drogowego.
5. Minister właściwy do spraw transportu w porozumieniu z ministrem właściwym
   do spraw finansów publicznych określi, w drodze rozporządzenia:

   1) tryb, sposób i termin wnoszenia opłat elektronicznych oraz ich
      rozliczania, w tym tryb dokonywania zwrotu nienależnie pobranych opłat
      elektronicznych,
   2) tryb i termin przekazywania opłat elektronicznych oraz kar pieniężnych, o
      których mowa w art. 13k ust. 1 i 2, na rachunek Krajowego Funduszu
      Drogowego,
   3) przypadki, w których ustanawiane jest zabezpieczenie należności z tytułu
      opłat elektronicznych, oraz formę i sposób jego ustalania,
   4) wysokość kaucji za wydawane korzystającemu z drogi publicznej urządzenie,
      o którym mowa w art. 13i ust. 3

   – mając na uwadze sprawny pobór opłat elektronicznych od użytkowników,
   efektywną obsługę i zabezpieczenie wpływów do Krajowego Funduszu Drogowego,
   o którym mowa w ustawie z dnia 27 października 1994 r. o autostradach
   płatnych oraz o Krajowym Funduszu Drogowym, oraz technologię wykorzystaną w
   systemie elektronicznego poboru opłat elektronicznych, a także uwzględniając
   koszty urządzenia, o którym mowa w art. 13i ust. 3.

Art.40b
-------

(uchylony).

Art.40c
-------

(uchylony).

Art.40d
-------

1. W przypadku nieterminowego uiszczenia opłat, o których mowa w art. 40
   ust. 3, opłat wynikających z umów zawieranych zgodnie z art. 22 ust. 2 i
   art. 39 ust. 7 oraz kar pieniężnych, o których mowa w art. 13k ust. 1 i 2,
   art. 29a ust. 1 i 2 i art. 40 ust. 12, pobiera się odsetki ustawowe.
2. Opłaty, o których mowa w art. 40 ust. 3, wraz z odsetkami za zwłokę, opłaty,
   o których mowa w art. 13f ust. 1, oraz kary pieniężne, o których mowa w
   art. 13k ust. 1 i 2, art. 29a ust. 1 i 2 i art. 40 ust. 12, wraz z odsetkami
   za zwłokę podlegają przymusowemu ściągnięciu w trybie określonym w
   przepisach ustawy z dnia 17 czerwca 1966 r. o postępowaniu egzekucyjnym w
   administracji.
3. Obowiązek uiszczenia opłat, o których mowa w art. 13f ust. 1 i art. 40
   ust. 3, oraz kar pieniężnych, o których mowa w art. 13k ust. 1 i 2, art. 29a
   ust. 1 i 2 oraz w art. 40 ust. 12, przedawnia się z upływem 5 lat, licząc od
   końca roku kalendarzowego, w którym opłaty lub kary powinny zostać
   uiszczone.

Art.41
------

1. Po drogach publicznych dopuszcza się ruch pojazdów o dopuszczalnym nacisku
   pojedynczej osi napędowej do 11,5 t, z zastrzeżeniem ust. 2 i 3.
2. Minister właściwy do spraw transportu ustala, w drodze rozporządzenia,
   wykaz:

   1) dróg krajowych oraz dróg wojewódzkich, po których mogą poruszać się
      pojazdy o dopuszczalnym nacisku pojedynczej osi do 10 t,
   2) dróg krajowych, po których mogą poruszać się pojazdy o dopuszczalnym
      nacisku pojedynczej osi do 8 t

   – mając na uwadze potrzebę ochrony dróg oraz zapewnienia ruchu tranzytowego.
3. Drogi wojewódzkie inne niż drogi określone na podstawie ust. 2 pkt 1, drogi
   powiatowe oraz drogi gminne stanowią sieć dróg, po których mogą poruszać się
   pojazdy o dopuszczalnym nacisku pojedynczej osi do 8 t.

Art.42
------

1. Zabrania się umieszczania nadziemnych urządzeń liniowych, w szczególności
   linii energetycznej, telekomunikacyjnej, rurociągu, taśmociągu, wzdłuż pasów
   drogowych, poza terenem zabudowy, w odległości mniejszej niż 5 m od granicy
   pasa, z zastrzeżeniem ust. 2 i 3.
2. W szczególnie uzasadnionych przypadkach związanych z potrzebami obronnymi i
   zadaniami na rzecz obronności kraju oraz ochrony środowiska umieszczenie
   urządzenia liniowego w odległości mniejszej niż określona w ust. 1 może
   nastąpić za zgodą zarządcy drogi, którą zarządca drogi wydaje inwestorowi
   przed uzyskaniem pozwolenia na budowę lub zgłoszeniem budowy albo
   wykonywania robót budowlanych. Przepis art. 38 ust. 3 stosuje się
   odpowiednio.
3. Urządzenia liniowe, o których mowa w ust. 1, mogą być umieszczane:

   1) na obszarach narażonych na niebezpieczeństwo powodzi - na skarpach
      nasypów drogowych, z wyjątkiem nasypów spełniających jednocześnie funkcję
      wałów przeciwpowodziowych, a w przypadku braku takiej możliwości - na
      krawędzi korony drogi,
   2) na terenach górskich, zalesionych i w parkach narodowych - w pasie
      drogowym poza koroną drogi

   – na warunkach określonych przez zarządcę drogi i za jego zgodą. Przepis
   art. 38 ust. 3 stosuje się odpowiednio.
4. Zarządca drogi wydaje zgodę, o której mowa w ust. 3, przed uzyskaniem przez
   inwestora pozwolenia na budowę lub zgłoszenia budowy albo wykonywania robót
   budowlanych.

Art.43
------

1. Obiekty budowlane przy drogach powinny być usytuowane w odległości od
   zewnętrznej krawędzi jezdni co najmniej:

+-------+---------------------------+-------------------+----------------------+
|L.p.   |Rodzaj drogi               |W terenie zabudowy |Poza terenem zabudowy |
|       |                           |                   |                      |
|       |                           |                   |                      |
+=======+===========================+===================+======================+
|1      |Autostrada                 |30 m               |50 m                  |
+-------+---------------------------+-------------------+----------------------+
|2      |Droga ekspresowa           |20 m               |40 m                  |
+-------+---------------------------+-------------------+----------------------+
|3      |Droga ogólnodostępna:      |                   |                      |
|       |  a) krajowa               |  a) 10 m          |a) 25 m               |
|       |  b) wojewódzka, powiatowa |  b) 8 m           |b) 20 m               |
|       |  c) gminna                |  c) 6 m           |c) 15 m               |
+-------+---------------------------+-------------------+----------------------+

2. W szczególnie uzasadnionych przypadkach usytuowanie obiektu budowlanego przy
   drodze, o której mowa w ust. 1 lp. 3 tabeli, w odległości mniejszej niż
   określona w ust. 1, może nastąpić wyłącznie za zgodą zarządcy drogi, wydaną
   przed uzyskaniem przez inwestora obiektu pozwolenia na budowę lub
   zgłoszeniem budowy albo wykonywania robót budowlanych. Przepis art. 38
   ust. 3 stosuje się odpowiednio.
3. Przepisu ust. 2 nie stosuje się przy sytuowaniu reklam poza terenem
   zabudowy.

Rozdział 4a —   Inteligentne systemy transportowe
*************************************************


Art.43a
-------

1. W przypadku podjęcia przez zainteresowane podmioty, w szczególności
   zarządców dróg publicznych poszczególnych kategorii, decyzji o wdrażaniu
   aplikacji ITS lub usług ITS, podmioty te przy ich wdrażaniu stosują przepisy
   wydane na podstawie ust. 3, mając na uwadze potrzeby w szczególności
   użytkowników ITS, w tym szczególnie zagrożonych uczestników ruchu drogowego.
2. Podmioty, o których mowa w ust. 1, przy wyborze i wdrażaniu aplikacji ITS i
   usług ITS stosują odpowiednio następujące zasady:

   1) skuteczności - rzeczywiste przyczynianie się do rozwiązania kluczowych
      wyzwań mających wpływ na transport drogowy w Europie, w szczególności
      zmniejszenia zatorów, ograniczenia emisji zanieczyszczeń, zwiększenia
      efektywności energetycznej transportu, osiągnięcia wyższych poziomów
      bezpieczeństwa i ochrony użytkowników ITS, w tym szczególnie zagrożonych
      uczestników ruchu drogowego;
   2) opłacalności - optymalizowanie stosunku kosztów do rezultatów mierzonych
      realizacją celów;
   3) proporcjonalności - zapewnianie różnych poziomów osiągalnej jakości usług
      ITS i ich wdrażania, z uwzględnieniem specyfiki lokalnej, regionalnej,
      krajowej i europejskiej, jeżeli jest to celowe;
   4) wspierania ciągłości usług ITS - zapewnianie ciągłości usług ITS na
      obszarze Unii Europejskiej, w szczególności w ramach transeuropejskiej
      sieci drogowej oraz, w miarę możliwości, na zewnętrznych granicach Unii
      Europejskiej; ciągłość usług powinna być zapewniona na poziomie
      dostosowanym do cech sieci łączących odpowiednio państwa, regiony, a
      także miasta z obszarami wiejskimi;
   5) zapewniania interoperacyjności - zapewnianie, aby ITS oraz procesy
      gospodarcze będące ich podstawą były zdolne do wymiany danych, informacji
      i wiedzy, aby umożliwić skuteczne świadczenie usług ITS;
   6) wspierania zgodności wstecznej - zapewnianie zdolności ITS do współpracy
      z istniejącymi systemami służącymi temu samemu celowi bez utrudniania
      rozwoju nowych technologii, jeżeli jest to celowe;
   7) poszanowania istniejącej infrastruktury krajowej i cech sieci drogowej -
      uwzględnianie naturalnych różnic między cechami sieci drogowych, w
      szczególności w zakresie natężenia ruchu oraz warunków drogowych
      związanych z pogodą;
   8) promowania równego dostępu - nieutrudnianie dostępu do aplikacji ITS i
      usług ITS szczególnie zagrożonym uczestnikom ruchu drogowego oraz ich
      niedyskryminowanie w zakresie tego dostępu;
   9) wspierania dojrzałości - wykazywanie, po dokonaniu odpowiedniej oceny
      ryzyka, odporności innowacyjnych ITS osiągniętej dzięki odpowiedniemu
      poziomowi zaawansowania technicznego i wykorzystania operacyjnego;
   10) zapewniania jakości określania czasu i położenia - wykorzystywanie
       infrastruktury satelitarnej lub dowolnej innej technologii zapewniającej
       równorzędne poziomy dokładności na potrzeby aplikacji ITS i usług ITS,
       które wymagają globalnych, nieprzerwanych, dokładnych i gwarantowanych
       usług związanych z określaniem czasu i położenia;
   11) ułatwiania intermodalności - uwzględnianie przy wdrażaniu ITS kwestii
       związanych z koordynacją różnych rodzajów transportu, jeżeli jest to
       celowe;
   12) poszanowania spójności - uwzględnianie istniejących zasad, kierunków
       polityki i działań Unii Europejskiej, które mają zastosowanie w zakresie
       ITS, w szczególności w dziedzinie normalizacji.

3. Minister właściwy do spraw transportu w porozumieniu z ministrem właściwym
   do spraw łączności może określić, w drodze rozporządzenia:

   1) po wydaniu przez Komisję Europejską specyfikacji w zakresie wdrażania
      inteligentnych systemów transportowych w obszarze transportu drogowego,
      szczegółowe wymagania techniczne lub operacyjne dla aplikacji ITS i usług
      ITS,
   2) sposoby wdrażania aplikacji ITS i usług ITS

   – mając na uwadze potrzebę zapewnienia skoordynowanego i spójnego wdrażania
   inteligentnych systemów transportowych w obszarze transportu drogowego,
   zaspokajanie w możliwie szerokim zakresie potrzeb użytkowników ITS oraz
   uwzględniając zasady, o których mowa w ust. 2.

Rozdział 5 — (uchylony)
***********************


Art.44
------

(uchylony)

Art.45
------

(uchylony)

Art.46
------

(uchylony)

Art.47
------

(uchylony)

Rozdział 6 — Zmiany w przepisach obowiązujących, przepisy przejściowe i końcowe
*******************************************************************************


Art.48
------

W kodeksie wykroczeń art. 98 otrzymuje brzmienie: (zmiany pominięte).

Art.49
------

W ustawie z dnia 19 grudnia 1975 r. o niektórych podatkach i opłatach
terenowych (Dz. U. Nr 45, poz. 229, z 1982 r. Nr 7, poz. 55, z 1983 r. Nr 6,
poz. 35 i z 1985 r. Nr 12, poz. 50) wprowadza się następujące zmiany: (zmiany
pominięte).

Art.50
------

Użyte w art. 27 pkt 3 ustawy z dnia 20 lipca 1983 r. o systemie rad narodowych
i samorządu terytorialnego (Dz. U. Nr 41, poz. 185 i Nr 62, poz. 286 oraz z
1984 r. Nr 21, poz. 100 i Nr 31, poz. 173) określenie "drogi państwowe" oznacza
w rozumieniu niniejszej ustawy drogi krajowe, określone w art. 14 ust. 1 pkt 1
lit. c).

Art.51
------

1. Z dniem wejścia ustawy w życie:

   1) grunty państwowe położone poza granicami miast, oddane osobom fizycznym w
      zamian za grunty zajęte pod budowę dróg publicznych, stają się z mocy
      prawa własnością tych osób; grunty takie położone w granicach miast
      przechodzą na własność lub w użytkowanie wieczyste osób fizycznych,
      stosownie do przepisów szczególnych;
   2) grunty oddane i zajęte pod drogi publiczne, wybudowane z udziałem czynu
      społecznego i istniejące w tym dniu, stają się z mocy prawa własnością
      Państwa.

2. Przepisu ust. 1 pkt 2 nie stosuje się do gruntów zajętych pod istniejące
   drogi publiczne, w odniesieniu do których zostało wszczęte postępowanie
   wywłaszczeniowe przed dniem wejścia ustawy w życie.

Art.52
------

Traci moc ustawa z dnia 29 marca 1962 r. o drogach publicznych (Dz. U. Nr 20,
poz. 90 oraz z 1971 r. Nr 12, poz. 115).

Art.53
------

Ustawa wchodzi w życie z dniem 1 października 1985 r.

Załączniki
**********

ZAŁĄCZNIK Nr 1 — (uchylony)
---------------------------


ZAŁĄCZNIK Nr 2 — (uchylony)
---------------------------
